import { TitleComponent } from "./shared/components/title/title.component";
import { NavigationComponent } from "./shared/components/navigation/navigation.component";
import { FooterComponent } from "./shared/components/footer/footer.component";
import { CommonModule } from "@angular/common";
import {NavigationEnd, Router, RouterOutlet} from "@angular/router";
import {Component, OnInit} from "@angular/core";
import {LoadingComponent} from "./loading/loading.component";
import {AuthService} from "./auth/service/auth.service";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    TitleComponent,
    NavigationComponent,
    FooterComponent,
    LoadingComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
  title = 'Fibernet';
  protected readonly navigator = navigator;
  showLoading : boolean = true;
  eventNumber: number = 0;

  constructor(private router: Router,
              protected authService: AuthService) {}
  ngOnInit() {

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd && this.eventNumber === 0) {
        this.showLoading = event.urlAfterRedirects === '/';
        setTimeout(() => {
          this.showLoading = false;
        }, 4000);
        this.eventNumber++;
        this.authService.loadJwtTokenFromLocalStorage();
      }
    });
  }

}
