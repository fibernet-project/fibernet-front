import {Component} from '@angular/core';
import {ButtonComponent} from "../shared/components/button/button.component";
import {RouterLink} from "@angular/router";
import {AuthService} from "../auth/service/auth.service";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    ButtonComponent,
    RouterLink,
    NgIf
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {

  constructor(protected authService: AuthService) {
  }
}
