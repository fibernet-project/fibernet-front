import {Component, OnInit} from '@angular/core';
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {NgIf} from "@angular/common";
import {FormBuilder, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {ButtonComponent} from "../button/button.component";
import {AuthService} from "../../../auth/service/auth.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router, RouterLink} from "@angular/router";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    NgIf,
    ReactiveFormsModule,
    ButtonComponent,
    RouterLink
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  hidePassword: boolean = true;
  protected requiredMessage: string = 'Champ obligatoire';
  protected errorMessage: string = 'Le mot de passe doit contenir au moins une minuscule, une majuscule, un caractère spécial et un chiffre';
  protected minLengthMessage: string = 'Ce champ doit contenir au moins 8 caractères';
  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private snackBar: MatSnackBar,
              private router: Router) {
  }
  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      username: [''
      //, [Validators.required, Validators.minLength(5)]
      ],
      password: [
        ''
        //,[
        //           Validators.required,
        //           Validators.minLength(8),
        //           Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/),
        //         ],
      ],
    });
  }
  togglePasswordVisibility(): void {
    this.hidePassword = !this.hidePassword;
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  onSubmit(): void {
    if (this.form.valid) {

    }
    const username = this.form.value.username;
    const password = this.form.value.password;
    this.authService.login(username, password).subscribe(
      (data) => {
        this.authService.loadProfile(data);
        this.showSuccessMessage("Connexion réussie !");
        this.router.navigateByUrl("/");
      },
      (error) => {
        if (error === 'Identifiant ou mot de passe incorrect') {
          this.showErrorMessage("Identifiant ou mot de passe incorrect");
        } else {
          this.showErrorMessage("Erreur lors de la connexion.");
        }
        console.error(error);
      }
    );
  }


}
