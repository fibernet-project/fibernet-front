import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorModule, PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-paginator',
  standalone: true,
  imports: [
    MatPaginatorModule
  ],
  templateUrl: './paginator.component.html',
  styleUrl: './paginator.component.scss'
})
export class PaginatorComponent {
  @Input() totalItems?: number;
  @Input() pageSize?: number;
  @Output() pageChanged = new EventEmitter<PageEvent>();
  @ViewChild(MatPaginator, { static: true }) paginator?: MatPaginator;


  onPageChange(event: PageEvent) {
    this.pageChanged.emit(event);
  }
}
