import { Component } from '@angular/core';
import {UpperCasePipe} from "@angular/common";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-title',
  standalone: true,
  imports: [
    UpperCasePipe,
    RouterLink
  ],
  templateUrl: './title.component.html',
  styleUrl: './title.component.scss'
})
export class TitleComponent {
  title : string = "fibernet";
}
