import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../auth/service/auth.service';
import {NgClass, NgIf, UpperCasePipe} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {ButtonComponent} from "../button/button.component";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-navigation',
  standalone: true,
  templateUrl: './navigation.component.html',
  imports: [
    UpperCasePipe,
    MatIconModule,
    ButtonComponent,
    NgIf,
    RouterLink,
    NgClass
  ],
  styleUrl: './navigation.component.scss'
})
export class NavigationComponent implements OnInit {

  title: string = 'Fibernet';
  showNavigation: boolean = false;
  isMobileScreen: boolean = false;

  constructor(protected authService: AuthService) {
  }

  ngOnInit() {
    this.checkIfMobileScreen();
  }

  toggleNavigation() {
    this.showNavigation = !this.showNavigation;
  }

  handleLogout() {
    this.authService.logout();
  }

  checkIfMobileScreen() {
    this.isMobileScreen = window.matchMedia('(max-width: 600px)').matches;
  }
}
