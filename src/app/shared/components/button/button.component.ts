import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgClass, NgIf} from "@angular/common";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {RouterLinkActive} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";

@Component({
  selector: 'app-button',
  standalone: true,
  imports: [
    NgClass,
    MatProgressSpinnerModule,
    NgIf,
    RouterLinkActive,
    MatIconModule
  ],
  templateUrl: './button.component.html',
  styleUrl: './button.component.scss'
})
export class ButtonComponent {

  @Input() buttonText: string = '';
  @Input() isNavigationButton: boolean = false;
  @Input() isSubmitButton: boolean = false;
  @Input() isResetButton: boolean = false;
  @Input() isUpdateButton: boolean = false;
  @Input() isDeleteButton: boolean = false;
  @Input() isBackButton: boolean = false;
  @Input() isLoginButton: boolean = false;
  @Input() isLogoutButton: boolean = false;
  @Output() onClick = new EventEmitter<void>();
  isClicked: boolean = false;

  get buttonClass(): string {
    if (this.isNavigationButton) {
      return 'navigation-button';
    } else if (this.isSubmitButton || this.isResetButton || this.isBackButton) {
      return 'form-button';
    } else if (this.isUpdateButton) {
      return 'update-button';
    } else if (this.isDeleteButton) {
      return 'delete-button';
    } else if (this.isLoginButton) {
      return 'login-button';
    } else if (this.isLogoutButton) {
      return 'logout-button';
    } else {
      return 'custom-button';
    }
  }

  get type(): string {
    if (this.isSubmitButton) {
      return 'submit';
    } else if (this.isResetButton) {
      return 'reset';
    } else {
      return 'button';
    }
  }

  handleClick(): void {
    if (this.isSubmitButton && !this.isClicked) { // && !this.isLoading
      this.isClicked = true;
      // this.isLoading = true;
      this.onClick.emit();
    } else if (!this.isSubmitButton) {
      this.onClick.emit();
    } else {
      this.isClicked = false;
      this.onClick.emit();
    }

  }
}
