import {Component} from '@angular/core';
import {MatDialogContent, MatDialogRef, MatDialogTitle} from "@angular/material/dialog";

@Component({
  selector: 'app-info-dialog-ref',
  standalone: true,
  imports: [
    MatDialogTitle,
    MatDialogContent
  ],
  templateUrl: './info-dialog-ref.component.html',
  styleUrl: './info-dialog-ref.component.scss'
})
export class InfoDialogRefComponent {
  constructor(public dialogRef: MatDialogRef<InfoDialogRefComponent>) {
  }
}
