import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoDialogRefComponent } from './info-dialog-ref.component';

describe('InfoDialogRefComponent', () => {
  let component: InfoDialogRefComponent;
  let fixture: ComponentFixture<InfoDialogRefComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [InfoDialogRefComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(InfoDialogRefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
