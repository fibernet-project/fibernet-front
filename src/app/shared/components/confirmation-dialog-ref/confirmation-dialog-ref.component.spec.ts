import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationDialogRefComponent } from './confirmation-dialog-ref.component';

describe('ConfirmationDialogRefComponent', () => {
  let component: ConfirmationDialogRefComponent;
  let fixture: ComponentFixture<ConfirmationDialogRefComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ConfirmationDialogRefComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ConfirmationDialogRefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
