import { Component } from '@angular/core';
import {MatDialogContent, MatDialogRef, MatDialogTitle} from "@angular/material/dialog";

@Component({
  selector: 'app-confirmation-dialog-ref',
  standalone: true,
  templateUrl: './confirmation-dialog-ref.component.html',
  imports: [
    MatDialogTitle,
    MatDialogContent
  ],
  styleUrls: ['./confirmation-dialog-ref.component.scss']
})
export class ConfirmationDialogRefComponent {
  constructor(public dialogRef: MatDialogRef<ConfirmationDialogRefComponent>) {}
}
