export enum InterventionType {
  STANDARD = 'Standard',
  FACADE = 'Façade',
  ROOM = 'Chambre',
  POST = 'Poteau'
}

export function displayInterventionTypeValue(interventionType: InterventionType | undefined): string {
  switch (interventionType?.valueOf()) {
    case 'STANDARD':
      return 'Standard';
    case 'FACADE':
      return 'Façade';
    case 'ROOM':
      return 'Chambre';
    case 'POST':
      return 'Poteau';
    default:
      return 'Inconnu';
  }
}
