
export enum Roles {
  ADMIN = 'ADMIN',
  MANAGER = 'MANAGER',
  RH = 'RH',
  USER = 'USER'
}
