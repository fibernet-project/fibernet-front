export enum ContractType {
  PERMANENT_CONTRACT = 'CDI',
  FIXED_TERM_CONTRACT = 'CDD',
  INTERIM = 'Intérim',
  INTERNSHIP = 'Stage'
}

export function displayContractTypeValue(contractType: ContractType | undefined): string {
  switch (contractType?.valueOf()) {
    case 'PERMANENT_CONTRACT' :
      return 'CDI';
    case 'FIXED_TERM_CONTRACT' :
      return 'CDD';
    case 'INTERIM' :
      return 'Intérim';
    case 'INTERNSHIP' :
      return 'Stage';
    default :
      return 'Inconnu'
  }
}
