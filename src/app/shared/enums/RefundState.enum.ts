export enum RefundState {
  ON_GOING = 'En cours',
  COMPLETED = 'Effectué'
}

export function displayRefundStateValue(refundState: RefundState | undefined): string {
  switch (refundState?.valueOf()) {
    case 'ON_GOING':
      return 'En cours';
    case 'COMPLETED':
      return 'Effectué';
    default:
      return 'Inconnu';
  }
}
