import {MutualType} from "./MutualType.enum";

export enum TechnicianRating {
  GOOD = 'Bon',
  AVERAGE = 'Moyen',
  BAD = 'Mauvais'
}

export function displayTechnicianRating(technicianRating: TechnicianRating | undefined): string {
  switch (technicianRating?.valueOf()) {
    case 'GOOD':
      return 'Bon';
    case 'AVERAGE':
      return 'Moyen';
    case 'BAD':
      return 'Mauvais';
    default:
      return 'Aucun';
  }
}
