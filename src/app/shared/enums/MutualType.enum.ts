export enum MutualType {
  INDIVIDUAL = 'Individuelle',
  FAMILY = 'Familiale'
}

export function displayMutualTypeValue(mutualType: MutualType | undefined): string {
  switch (mutualType?.valueOf()) {
    case 'INDIVIDUAL':
      return 'Individuelle';
    case 'FAMILY':
      return 'Familiale';
    default:
      return 'Aucun';
  }
}
