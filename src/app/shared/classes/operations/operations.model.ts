import {BaseEntity} from "../bases/base-entity.model";
import {Employee} from "../../../employee/classes/employee/employee.model";

export abstract class Operations extends BaseEntity {

  date?: Date;
  employee?: Employee;

  protected constructor(data: Partial<Operations>) {
    super();
    Object.assign(this, data);
  }
}
