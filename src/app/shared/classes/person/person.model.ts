import {BaseEntity} from "../bases/base-entity.model";
import {Email} from "../email/email.model";

export abstract class Person extends BaseEntity {

  lastName?: string;
  firstName?: string;
  email?: Email;
  constructor(data: Partial<Person>) {
    super();
    Object.assign(this, data);
  }
}
