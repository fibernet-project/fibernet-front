import {Person} from "../person/person.model";
import {Active} from "../active/active.model";
import {PhoneNumber} from "../phoneNumber/phone-number.model";

export abstract class Worker extends Person {
  birthDate?: Date;
  foreignWorker?: boolean;
  active?: Active;
  phoneNumber?: PhoneNumber;
}
