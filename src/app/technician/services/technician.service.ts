import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Technician} from "../classes/technician/technician.model";
import {Observable, throwError} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TechnicianService {

  private apiUrl = environment.apiUrl;
  private endPoint : string = '/technicians';
  private errorMessage: string = "Le technicien doit avoir un ID pour être mis à jour.";
  constructor(private httpClient: HttpClient) { }

  public addTechnician(technician: Technician) : Observable<Object> {
    return this.httpClient.post(`${this.apiUrl + this.endPoint}`, technician);
  }

  public getAllTechnicians(): Observable<Technician[]> {
    return this.httpClient.get<Technician[]>(`${this.apiUrl + this.endPoint}`);
  }

  public getTechnicianById(id: string) : Observable<Technician> {
    return this.httpClient.get<Technician>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  public deleteTechnicianById(id: string): Observable<void> {
    return this.httpClient.delete<void>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  public updateTechnician(technician: Technician, id: string | null): Observable<Object> {

    if (id === null) {
      console.error(this.errorMessage);
      return throwError(this.errorMessage);
    }
    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}`, technician);
  }

  public updateAuditTechnician(technician: Technician, id: string | null): Observable<Object> {
    if (id === null) {
      console.error(this.errorMessage);
      return throwError(this.errorMessage);
    }
    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}/audit`, technician);
  }

  public updateIsActive(technician: Technician, id: string | null): Observable<Object> {
    if (id === null) {
      console.error(this.errorMessage);
      return throwError(this.errorMessage);
    }
    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}/isActive`, technician);
  }
}
