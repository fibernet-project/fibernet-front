import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTechnicianComponent } from './search-technician.component';

describe('SearchTechnicianComponent', () => {
  let component: SearchTechnicianComponent;
  let fixture: ComponentFixture<SearchTechnicianComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SearchTechnicianComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SearchTechnicianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
