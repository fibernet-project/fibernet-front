import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {NgForOf, NgIf} from "@angular/common";
import {RouterLink} from "@angular/router";
import {Technician} from "../../classes/technician/technician.model";
import {TechnicianService} from "../../services/technician.service";
import {map, startWith} from "rxjs/operators";

@Component({
  selector: 'app-search-technician',
  standalone: true,
  imports: [
    MatInputModule,
    MatIconModule,
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    RouterLink
  ],
  templateUrl: './search-technician.component.html',
  styleUrl: './search-technician.component.scss'
})
export class SearchTechnicianComponent implements OnInit {

  searchCtrl!: FormControl;
  searchResults: Technician[] = [];

  constructor(private technicianService: TechnicianService,
              private formBuilder: FormBuilder) {
  }
  ngOnInit(): void {
    this.initForm();
    this.initObservables();
  }

  private initForm(): void {
    this.searchCtrl = this.formBuilder.control('');
  }

  private initObservables(): void {
    const search$ = this.searchCtrl.valueChanges.pipe(
      startWith(this.searchCtrl.value),
      map(value => value.toLowerCase())
    );

    this.technicianService.getAllTechnicians().subscribe(technicians => {
      search$.subscribe(search => {
        this.searchResults = technicians.filter(technician =>
          (technician.firstName && technician.firstName.toLowerCase().includes(search)) ||
          (technician.lastName && technician.lastName.toLowerCase().includes(search))
        );
      });
    });
  }

}
