import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {JsonPipe, NgForOf, NgIf} from "@angular/common";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {Contractor} from "../../../contractor/classes/contractor/contractor.model";
import {ContractorService} from "../../../contractor/services/contractor.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {Technician} from "../../classes/technician/technician.model";
import {TechnicianService} from "../../services/technician.service";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatDialog} from "@angular/material/dialog";
import {InfoDialogComponent} from "../../../shared/components/info-dialog/info-dialog.component";
import {MatRadioModule} from "@angular/material/radio";

@Component({
  selector: 'app-add-technician',
  standalone: true,
    imports: [
        ReactiveFormsModule,
        MatInputModule,
        JsonPipe,
        ButtonComponent,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        NgIf,
        NgForOf,
        MatIconModule,
        MatButtonModule,
        MatRadioModule
    ],
  templateUrl: './add-technician.component.html',
  styleUrl: './add-technician.component.scss'
})
export default class AddTechnicianComponent implements OnInit {
  form!: FormGroup;
  contractors: Contractor[] = [];
  protected requiredMessage: string = 'Champ obligatoire';
  protected onlyNumberMessage: string = 'Veuillez saisir des chiffres uniquement';
  protected lengthTelNumberMessage: string = 'Le numéro doit avoir 10 chiffres';
  protected emptyContractorMessage: string = 'Aucune entreprise à afficher. Veuillez aller dans le menu "Prestataires".';
  protected invalidEmailMessage: string = 'Adresse email invalide';
  hidePassword: boolean = true;

  constructor(private fb: FormBuilder,
              private contractorService: ContractorService,
              private snackBar: MatSnackBar,
              private router: Router,
              private technicianService: TechnicianService,
              private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.initForm();
    this.getAllCompanies();
  }

  private initForm(): void {

    this.form = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      birthDate: ['', [Validators.required]],
      idTechnician: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      login: ['', [Validators.required]],
      area: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      productionStart: ['', [Validators.required]],
      productionEnd: [''],
      contractor: [null, [Validators.required]],
      password: [''],
      comment: [''],
      foreignWorker: [true],
      active: this.fb.group({
        isActive: [true]
      }),
      phoneNumber: this.fb.group({
        phoneNumber: [
          '',
          [
            Validators.pattern(/^[0-9]+$/),
            Validators.minLength(10),
            Validators.maxLength(10),
          ],
        ]
      }),
      email: this.fb.group({
        email: ['', [Validators.email]]
      }),
      hasDPAE: ['true', [Validators.required]]
    });
  }


  getAllCompanies() {
    this.contractorService.getAllContractors().subscribe(
      (data: Contractor[]) => {
        this.contractors = data;
      },
      (error) => {
        console.error('Erreur lors de la récupération des données', error);
      }
    );
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  saveTechnician(): Observable<any> {
    const technicianData: Technician = this.form.value;
    return this.technicianService.addTechnician(technicianData);
  }

  togglePasswordVisibility(): void {
    this.hidePassword = !this.hidePassword;
  }

  onSubmit() {
    if (this.form.valid) {
      this.saveTechnician().subscribe(
        (response) => {
          console.log('Réponse du serveur : ', response);
          this.showSuccessMessage('Technicien ajouté avec succès !');
          this.router.navigate(['technician-list']);
        },
        (error) => {
          console.error('Erreur lors de la requête', error);
          this.showErrorMessage('Erreur lors de l\'ajout du technicien.');
        }
      );
    } else if (this.form.get('contractor')?.value === null) {
      const dialogMessage: string = "Merci d'ajouter un prestataire avant d'ajouter un technicien.";
      const dialogRef = this.dialog.open(InfoDialogComponent, {
        width: '250px',
        data: {
          title: 'Attention',
          message: dialogMessage,
          buttonMessage: "J'ai compris"
        }
      });
    }
  }
}
