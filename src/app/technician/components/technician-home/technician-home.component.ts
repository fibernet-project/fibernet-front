import { Component } from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import { Router, RouterLink } from "@angular/router";

@Component({
  selector: 'app-technician-home',
  standalone: true,
    imports: [
        ButtonComponent,
        RouterLink
    ],
  templateUrl: './technician-home.component.html',
  styleUrl: './technician-home.component.scss'
})
export class TechnicianHomeComponent {

  constructor(private router: Router) {
  }

  addTechnician() {
    this.router.navigate(['/add-technician']);
  }

  technicianList() {
    this.router.navigate(['/technician-list']);
  }

  searchTechnician() {
    this.router.navigate(['search-technician']);
  }
}
