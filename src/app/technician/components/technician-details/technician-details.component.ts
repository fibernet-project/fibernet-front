import {Component, OnInit} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {DatePipe, Location, NgForOf, NgIf} from "@angular/common";
import {Technician} from "../../classes/technician/technician.model";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {
  ConfirmationDialogComponent
} from "../../../shared/components/confirmation-dialog/confirmation-dialog.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatDialog} from "@angular/material/dialog";
import {TechnicianService} from "../../services/technician.service";
import {AuthService} from "../../../auth/service/auth.service";

@Component({
  selector: 'app-technician-details',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    DatePipe,
    ButtonComponent,
    RouterLink,
    ConfirmationDialogComponent
  ],
  templateUrl: './technician-details.component.html',
  styleUrl: './technician-details.component.scss'
})
export class TechnicianDetailsComponent implements OnInit {

  technicianId: string | null = null;
  technicianDetails: Technician | undefined;
  dateFormat: string = 'dd/MM/YYYY';

  constructor(private route: ActivatedRoute,
              private technicianService: TechnicianService,
              private router: Router,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private location: Location,
              protected authService: AuthService) {
  }

  ngOnInit(): void {
    this.technicianId = this.route.snapshot.paramMap.get('id');
    if (this.technicianId) {
      this.technicianService.getTechnicianById(this.technicianId).subscribe(
        (details) => {
          this.technicianDetails = details;
        },
        (error) => {
          console.error('Error fetching technician', error);
        }
      );
    }
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  deleteTechnician() {
    if (this.technicianId) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir supprimer ce technicien ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.technicianService.deleteTechnicianById(this.technicianId!).subscribe(
            () => {
              this.showSuccessMessage('Technicien supprimé avec succès');
              this.router.navigate(['technician-list']);
            },
            (error) => {
              console.error('Erreur lors de la suppression du technicien.', error);
              this.showErrorMessage('Erreur lors de la suppression du technicien');
            }
          );
        }
      });
    }
  }

  protected goBack() {
    this.location.back();
  }

  updateStateTechnician() {
    if (this.technicianId) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir modifier ce technicien ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {

          let updatedTechnician: Technician = {

            ...this.technicianDetails!,
            active: {
              isActive: !this.technicianDetails?.active?.isActive,
            }
          };

          this.technicianService.updateIsActive(updatedTechnician, this.technicianId).subscribe(
            () => {

              if (this.technicianDetails) {

                this.technicianDetails.active!.isActive = !this.technicianDetails.active!.isActive;

              }
              this.showSuccessMessage('Technicien mis à jour avec succès');
            },
            (error) => {
              console.error('Erreur lors de la mise à jour du technicien.', error);
              this.showErrorMessage('Erreur lors de la mise à jour du technicien');
            }
          );
        }
      });
    }
  }

}
