import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { TechnicianService } from '../../services/technician.service';
import {Contractor} from "../../../contractor/classes/contractor/contractor.model";
import {MatInputModule} from "@angular/material/input";
import {JsonPipe, Location, NgForOf, NgIf} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {MatButtonModule} from "@angular/material/button";
import {Technician} from "../../classes/technician/technician.model";
import {ContractorService} from "../../../contractor/services/contractor.service";
import {MatCardModule} from "@angular/material/card";
import {MatRadioModule} from "@angular/material/radio";

@Component({
  selector: 'app-update-technician',
  templateUrl: './update-technician.component.html',
  styleUrls: ['./update-technician.component.scss'],
    imports: [
        ReactiveFormsModule,
        MatInputModule,
        NgIf,
        MatIconModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        ButtonComponent,
        MatButtonModule,
        NgForOf,
        MatNativeDateModule,
        MatCardModule,
        JsonPipe,
        MatRadioModule
    ],
  standalone: true
})
export class UpdateTechnicianComponent implements OnInit {
  technicianId: string | null = null;
  technicianDetails: Technician | undefined;
  technicianForm: FormGroup;
  hidePassword: boolean = true;
  contractors: Contractor[] = [];
  protected requiredMessage: string = 'Champ obligatoire';
  protected onlyNumberMessage: string = 'Veuillez saisir des chiffres uniquement';
  protected lengthTelNumberMessage: string = 'Le numéro doit avoir 10 chiffres';
  protected invalidEmailMessage: string = 'Adresse email invalide';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private technicianService: TechnicianService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private contractorService: ContractorService,
    private location: Location
  ) {
    this.technicianForm = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      area: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      idTechnician: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      login: ['', Validators.required],
      password: ['', Validators.required],
      birthDate: [''],
      productionStart: [''],
      productionEnd: [''],
      comment: [''],
      contractor: [''],
      foreignWorker: [true],
      active: this.formBuilder.group({
        isActive: ['']
      }),
      phoneNumber: this.formBuilder.group({
        phoneNumber: [
          '',
          [
            Validators.pattern(/^[0-9]+$/),
            Validators.minLength(10),
            Validators.maxLength(10),
          ],
        ]
      }),
      email: this.formBuilder.group({
        email: ['', [Validators.email]]
      }),
      hasDPAE: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {

    this.technicianId = this.route.snapshot.paramMap.get('id');
    if (this.technicianId) {
      this.technicianService.getTechnicianById(this.technicianId).subscribe(
        (details) => {
          this.technicianDetails = details;
          this.contractorService.getAllContractors().subscribe(
            (contractorDetails) => {
              this.contractors = contractorDetails;
            },
            (error) => {
              console.error('Erreur lors de la récupération des détails du contractor', error);
            }
          );

          this.initializeForm(details);
        },
        (error) => {
          console.error('Erreur lors de la récupération des détails du technicien.', error);
        }
      );
    } else {
      console.error('ID du technicien non trouvé dans l\'URL.');
    }
  }

  private initializeForm(details: Technician): void {

    this.technicianForm.patchValue({
      lastName: details.lastName,
      firstName: details.firstName,
      area: details.area,
      idTechnician: details.idTechnician,
      login: details.login,
      password: details.password,
      birthDate: details.birthDate ? new Date(details.birthDate) : null,
      productionStart: details.productionStart ? new Date(details.productionStart) : null,
      productionEnd: details.productionEnd ? new Date(details.productionEnd) : null,
      comment: details.comment,
      contractor: details.contractor,
      foreignWorker: details.foreignWorker,
      phoneNumber: {
        phoneNumber: details.phoneNumber?.phoneNumber
      },
      email: {
        email: details.email?.email
      },
      hasDPAE: details.hasDPAE
    });
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar'],
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar'],
    });
  }

  protected goBack(): void {
    this.location.back();
  }

  togglePasswordVisibility(): void {
    this.hidePassword = !this.hidePassword;
  }

  onSubmit() {
    if (this.technicianForm.valid) {
      let formValue = this.technicianForm.value;

      let updatedTechnician: Technician = {
        lastName: formValue.lastName,
        firstName: formValue.firstName,
        area: formValue.area,
        idTechnician: formValue.idTechnician,
        login: formValue.login,
        password: formValue.password,
        birthDate: formValue.birthDate,
        productionStart: formValue.productionStart,
        productionEnd: formValue.productionEnd,
        comment: formValue.comment,
        contractor: formValue.contractor,
        foreignWorker: formValue.foreignWorker,
        phoneNumber: {
          phoneNumber: formValue.phoneNumber.phoneNumber
        },
        email: {
          email: formValue.email.email
        },
        active: {
          isActive: formValue.active.isActive
        },
        hasDPAE: formValue.hasDPAE
      };

      this.technicianService.updateTechnician(updatedTechnician, this.technicianId).subscribe(
        () => {
          this.showSuccessMessage('Technicien mis à jour avec succès');
          this.router.navigate(['technician-list/']);
        },
        (error) => {
          console.error('Erreur lors de la mise à jour du technicien.', error);
          this.showErrorMessage("Erreur lors de la modification du technicien");
        }
      );
    }
  }
}
