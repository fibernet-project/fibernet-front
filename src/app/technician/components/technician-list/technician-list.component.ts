import {Component, OnInit, ViewChild} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {NgForOf, NgIf, NgStyle} from "@angular/common";
import {TechnicianService} from "../../services/technician.service";
import {Technician} from "../../classes/technician/technician.model";
import {RouterLink} from "@angular/router";
import {PaginatorComponent} from "../../../shared/components/paginator/paginator.component";
import {PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-technician-list',
  standalone: true,
    imports: [
        ButtonComponent,
        NgForOf,
        NgIf,
        RouterLink,
        NgStyle,
        PaginatorComponent
    ],
  templateUrl: './technician-list.component.html',
  styleUrl: './technician-list.component.scss'
})
export class TechnicianListComponent implements OnInit {
  @ViewChild(PaginatorComponent) paginator: PaginatorComponent | undefined;
  technicians: Technician[] = [];
  totalTechnicians: number = 0;
  pageSize: number = 5;
  currentPage: number = 1;
  currentPageData: Technician[] = [];
  constructor(private technicianService: TechnicianService) {
  }

  ngOnInit(): void {
    this.getTechnicians();
  }

  getTechnicians() {
    this.technicianService.getAllTechnicians().subscribe(
      (data: Technician[]) => {
        this.technicians = data;
        this.totalTechnicians = data.length;
        this.updatePageData();
      },
      (error) => {
        console.error('Erreur lors de la récupération des techniciens', error);
      }
    );
  }

  onPageChanged(event: PageEvent) {
    this.currentPage = event.pageIndex + 1;
    this.updatePageData();
  }

  updatePageData() {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    this.currentPageData = this.technicians.slice(startIndex, endIndex);
  }

}
