import {TechnicianRating} from "../../../shared/enums/TechnicianRating.enum";

export class AuditInformations {

  auditComment?: string;
  professionalCard?: boolean;
  qualifications?: boolean;

  // AIPR: Authorization for Intervention in the Proximity of Networks
  aiprAuthorization?: boolean;

  // PPR: Prevention Plan for Networks
  pprPlan?: boolean;
  vehicleMarking?: boolean;
  personalProtectiveEquipment?: boolean;
  equipment?: boolean;
  workMethodology?: boolean;
  process?: boolean;
  technicianRating?: TechnicianRating;
  hasCherryPicker?: boolean;

  // CACES: Certificate of Aptitude for Safe Driving
  hasCACES?: boolean;

  // VGP: Periodic General Inspection
  hasVGP?: boolean;

  // Indicates whether there is a flying cherry picker in the team
  hasFlyingCherryPicker?: boolean;
}
