import { Contractor } from "../../../contractor/classes/contractor/contractor.model";
import { Person } from "../../../shared/classes/person/person.model";
import {AuditInformations} from "../audit-informations/audit-informations.model";
import {Worker} from "../../../shared/classes/worker/worker.model";


export class Technician extends Worker {
  // Different from id in the database
  idTechnician?: number;
  login?: string;
  password?: string;
  area?: number;
  contractor?: Contractor;
  productionStart?: Date;
  productionEnd?: Date;
  comment?: string;
  auditInformations?: AuditInformations = new AuditInformations();
  hasDPAE?: boolean;


  constructor(data: Partial<Technician>) {
    super(data);
    Object.assign(this, data);
  }
}
