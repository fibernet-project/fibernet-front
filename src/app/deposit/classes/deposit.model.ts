import {Employee} from "../../employee/classes/employee/employee.model";
import {Operations} from "../../shared/classes/operations/operations.model";

export class Deposit extends Operations {

  amount?: number;

  constructor(data: Partial<Employee>) {
    super(data);
    Object.assign(this, data);
  }
}
