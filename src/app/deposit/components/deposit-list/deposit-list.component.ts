import {Component, OnInit} from '@angular/core';
import {DepositService} from "../../services/deposit.service";
import {AsyncPipe, DatePipe} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {
  ConfirmationDialogComponent
} from "../../../shared/components/confirmation-dialog/confirmation-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {RouterLink} from "@angular/router";
import {Deposit} from "../../classes/deposit.model";
import {ButtonComponent} from "../../../shared/components/button/button.component";


@Component({
  selector: 'app-deposit-list',
  standalone: true,
  imports: [
    DatePipe,
    AsyncPipe,
    MatIconModule,
    ButtonComponent,
    RouterLink
  ],
  templateUrl: './deposit-list.component.html',
  styleUrl: './deposit-list.component.scss'
})
export class DepositListComponent implements OnInit {

  deposits: Deposit[] = [];
  dateFormat: string = 'dd/MM/YYYY';
  constructor(private depositService: DepositService,
              private dialog: MatDialog,
              private snackBar: MatSnackBar) {
  }
  ngOnInit() {
    this.getDeposits();
  }

  getDeposits(): void {
    this.depositService.getAllDeposits().subscribe(
      (data) => {
        this.deposits = data;
      },
      (error) => {
        console.error('Erreur lors de la récupération des acomptes:', error);
      }
    );
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }
  deleteDeposit(id: number | undefined) {
    if (id) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir supprimer cet acompte ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.depositService.deleteDepositById(id!).subscribe(
            () => {
              this.showSuccessMessage('Acompte supprimé avec succès');
              this.getDeposits();
            },
            (error) => {
              console.error('Erreur lors de la suppression de l\'acompte.', error);
              this.showErrorMessage('Erreur lors de la suppression de l\'acompte');
            }
          );
        }
      });
    }
  }

}
