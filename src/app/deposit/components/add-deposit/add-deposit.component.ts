import {Component, OnInit} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {MatRadioModule} from "@angular/material/radio";
import {MatSelectModule} from "@angular/material/select";
import {NgForOf, NgIf} from "@angular/common";
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {Employee} from "../../../employee/classes/employee/employee.model";
import {EmployeeService} from "../../../employee/services/employee.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {DepositService} from "../../services/deposit.service";
import {Deposit} from "../../classes/deposit.model";

@Component({
  selector: 'app-add-deposit',
  standalone: true,
  imports: [
    ButtonComponent,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatRadioModule,
    MatSelectModule,
    NgIf,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgForOf
  ],
  templateUrl: './add-deposit.component.html',
  styleUrl: './add-deposit.component.scss'
})
export class AddDepositComponent implements OnInit {
  form!: FormGroup;
  employees: Employee[] = [];
  protected requiredMessage: string = 'Champ obligatoire';
  protected onlyNumberMessage: string = 'Veuillez saisir des chiffres uniquement';
  protected emptyEmployeeMessage: string = 'Aucun employé à afficher. Veuillez aller dans le menu "Employés".';

  constructor(private formBuilder: FormBuilder,
              private employeeService: EmployeeService,
              private snackBar: MatSnackBar,
              private router: Router,
              private depositService: DepositService) {
  }

  ngOnInit() {
    this.initForm();
    this.getAllEmployees();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      date: ['', [Validators.required]],
      amount: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      employee: ['', [Validators.required]]
    })
  }

  getAllEmployees() {
    this.employeeService.getAllEmployees().subscribe(
      (data) => {
        this.employees = data;
      },
      (error) => {
        console.error('Erreur lors de la récupération des données', error);
      });
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  saveDeposit(): Observable<any> {
    const depositData: Deposit = this.form.value;
    return this.depositService.addDeposit(depositData);
  }

  onSubmit() {
    if (this.form.valid) {
      this.saveDeposit().subscribe(
        (response) => {
          console.log('Réponse du serveur : ', response);
          this.showSuccessMessage('Acompte ajouté avec succès !');
          this.router.navigate(['deposit-list']);
        },
        (error) => {
          console.error('Erreur lors de la requête', error);
          this.showErrorMessage('Erreur lors de l\'ajout de l\'acompte.');
        }
      );
    }
  }
}
