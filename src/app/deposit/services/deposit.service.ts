import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Deposit} from "../classes/deposit.model";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DepositService {

  private apiUrl = environment.apiUrl;
  private endPoint : string = '/deposits';

  constructor(private httpClient: HttpClient) { }

  public addDeposit(deposit: Deposit): Observable<Object> {
    return this.httpClient.post(`${this.apiUrl + this.endPoint}`, deposit);
  }

  public getAllDeposits(): Observable<Deposit[]> {
    return this.httpClient.get<Deposit[]>(`${this.apiUrl + this.endPoint}`);
  }

  public deleteDepositById(id: number) {
    return this.httpClient.delete<void>(`${this.apiUrl + this.endPoint}/${id}`);
  }
}
