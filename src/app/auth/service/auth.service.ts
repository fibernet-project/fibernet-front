import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { jwtDecode } from 'jwt-decode';
import { Router } from '@angular/router';
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAuthenticated: boolean = false;
  roles: any;
  username: any;
  accessToken!: any;
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private router: Router) {
    this.loadJwtTokenFromLocalStorage();
  }

  public login(username: string, password: string): Observable<any> {
    let options = {
      headers: new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded")
    }
    let params = new HttpParams()
      .set("username", username)
      .set("password", password);
    return this.http.post(`${this.apiUrl}/auth/login`, params, options).pipe(
      tap(data => this.handleLoginSuccess(data)),
      catchError(this.handleError)
    );
  }

  private handleLoginSuccess(data: any): void {
    this.loadProfile(data);
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.status === 401) {
      return throwError("Identifiant ou mot de passe incorrect");
    } else {
      return throwError("Erreur lors de la connexion");
    }
  }

  loadProfile(data: any) {
    this.isAuthenticated = true;
    this.accessToken = data['access-token'];
    let decodedJwt: any = jwtDecode(this.accessToken);
    this.username = decodedJwt.sub;
    this.roles = decodedJwt.scope;
    const expirationTime = decodedJwt.exp * 1000; // conversion in millis
    if (expirationTime < Date.now()) {
      this.logout();
      return;
    }

    window.localStorage.setItem("jwt-token", this.accessToken);
  }

  logout() {
    this.isAuthenticated = false;
    this.accessToken = undefined;
    this.username = undefined;
    this.roles = undefined;
    window.localStorage.removeItem("jwt-token");
    this.router.navigateByUrl("/");
  }

  loadJwtTokenFromLocalStorage() {
    let token = window.localStorage.getItem("jwt-token");
    if (token) {
      this.loadProfile({"access-token": token});
    }
  }
}
