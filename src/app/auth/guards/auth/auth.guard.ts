import { inject } from "@angular/core";
import { Router } from "@angular/router";
import {AuthService} from "../../service/auth.service";


export const AuthGuard = () => {
  const auth = inject(AuthService);
  const router = inject(Router);

  if(auth.roles && (auth.roles.includes('ADMIN') || auth.roles.includes('USER') ||
    auth.roles.includes('MANAGER') || auth.roles.includes('RH'))) {
    return true;
  } else {
    router.navigateByUrl("/not-authorized");
    return false;
  }
}

