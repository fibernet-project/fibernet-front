import {Router} from '@angular/router';
import {inject} from "@angular/core";
import {AuthService} from "../../service/auth.service";

export const UserGuard = () => {
  const auth = inject(AuthService);
  const router = inject(Router);

  if(auth.roles && auth.roles.includes('USER') || auth.roles.includes('ADMIN') || auth.roles.includes('MANAGER')) {
    return true;
  } else {
    router.navigateByUrl("/not-authorized");
    return false;
  }
}
