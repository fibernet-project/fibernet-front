import {Routes} from '@angular/router';
import {AdminGuard} from "./auth/guards/admin/admin.guard";
import {RhGuard} from "./auth/guards/rh/rh.guard";
import {UserGuard} from "./auth/guards/user/user.guard";
import {AuthGuard} from "./auth/guards/auth/auth.guard";

export const routes: Routes = [
  {
    path: '',
    title: 'Accueil',
    loadComponent: () => import('./home/home.component').then(m => m.HomeComponent)
  },
  {
    path: 'contractor-home',
    title: 'Prestataire',
    canActivate: [RhGuard],
    loadComponent: () => import('./contractor/components/contractor-home/contractor-home.component')
      .then(m => m.ContractorHomeComponent)
  },
  {
    path: 'employee-home',
    title: 'Employé',
    canActivate: [RhGuard],
    loadComponent: () => import('./employee/components/employee-home/employee-home.component')
      .then(m => m.EmployeeHomeComponent)
  },
  {
    path: 'technician-home',
    title: 'Technicien',
    canActivate: [UserGuard],
    loadComponent: () => import('./technician/components/technician-home/technician-home.component')
      .then(m => m.TechnicianHomeComponent)
  },
  {
    path: 'add-employee',
    title: 'Ajouter d\'un employé',
    canActivate: [RhGuard],
    loadComponent: () => import('./employee/components/add-employee/add-employee.component')
      .then(m => m.AddEmployeeComponent)
  },
  {
    path: 'add-technician',
    title: 'Ajout d\'un technicien',
    canActivate: [UserGuard],
    loadComponent: () => import('./technician/components/add-technician/add-technician.component')
  },
  {
    path: 'employee-list',
    title: 'Liste des employés',
    canActivate: [RhGuard],
    loadComponent: () => import('./employee/components/employee-list/employee-list.component')
      .then(m => m.EmployeeListComponent)
  },
  {
    path: 'search-employee',
    title: 'Recherche d\'un employé',
    canActivate: [RhGuard],
    loadComponent: () => import('./employee/components/search-employee/search-employee.component')
      .then(m => m.SearchEmployeeComponent)
  },
  {
    path: 'employee-details/:id',
    title: 'Détails d\'un employé',
    canActivate: [RhGuard],
    loadComponent: () => import('./employee/components/employee-details/employee-details.component')
      .then(m => m.EmployeeDetailsComponent)
  },
  {
    path: 'update-employee/:id',
    title: 'Modification d\'un employé',
    canActivate: [RhGuard],
    loadComponent: () => import('./employee/components/update-employee/update-employee.component')
      .then(m => m.UpdateEmployeeComponent)
  },
  {
    path: 'contractor-list',
    title: 'Liste des prestataires',
    canActivate: [RhGuard],
    loadComponent: () => import('./contractor/components/contractor-list/contractor-list.component')
      .then(m => m.ContractorListComponent)
  },
  {
    path: 'add-contractor',
    title: 'Ajout d\'un prestataire',
    canActivate: [RhGuard],
    loadComponent: () => import('./contractor/components/add-contractor/add-contractor.component')
      .then(m => m.AddContractorComponent)
  },
  {
    path: 'contractor-details/:id',
    title: 'Détails d\'un prestataire',
    canActivate: [RhGuard],
    loadComponent: () => import('./contractor/components/contractor-details/contractor-details.component')
      .then(m => m.ContractorDetailsComponent)
  },
  {
    path: 'update-contractor/:id',
    title: 'Modification d\'un prestataire',
    canActivate: [RhGuard],
    loadComponent: () => import('./contractor/components/update-contractor/update-contractor.component')
      .then(m => m.UpdateContractorComponent)
  },
  {
    path: 'technician-list',
    title: 'Liste des techniciens',
    canActivate: [UserGuard],
    loadComponent: () => import('./technician/components/technician-list/technician-list.component')
      .then(m => m.TechnicianListComponent)
  },
  {
    path: 'technician-details/:id',
    title: 'Détails d\'un technicien',
    canActivate: [UserGuard],
    loadComponent: () => import('./technician/components/technician-details/technician-details.component')
      .then(m => m.TechnicianDetailsComponent)
  },
  {
    path: 'search-technician',
    title: 'Recherche d\'un technicien',
    canActivate: [UserGuard],
    loadComponent: () => import('./technician/components/search-technician/search-technician.component')
      .then(m => m.SearchTechnicianComponent)
  },
  {
    path: 'update-technician/:id',
    title: 'Modification d\'un technicien',
    canActivate: [UserGuard],
    loadComponent: () => import('./technician/components/update-technician/update-technician.component')
      .then(m => m.UpdateTechnicianComponent)
  },
  {
    path: 'login',
    title: 'Connexion',
    loadComponent: () => import('./shared/components/login/login.component').then(m => m.LoginComponent)
  },
  {
    path: 'audit-home',
    title: 'Audit',
    canActivate: [UserGuard],
    loadComponent: () => import('./audit/components/audit-home/audit-home.component')
      .then(m => m.AuditHomeComponent)
  },
  {
    path: 'audit-technician/:id',
    title: 'Audit d\'un technicien',
    canActivate: [UserGuard],
    loadComponent: () => import('./audit/components/audit-technician/audit-technician.component')
      .then(m => m.AuditTechnicianComponent)
  },
  {
    path: 'audit-search-technician',
    title: 'Recherche d\'un technicien',
    canActivate: [UserGuard],
    loadComponent: () => import('./audit/components/audit-search-technician/audit-search-technician.component')
      .then(m => m.AuditSearchTechnicianComponent)
  },
  {
    path: 'audit-technician-details/:id',
    title: 'Détails d\'un technicien',
    canActivate: [UserGuard],
    loadComponent: () => import('./audit/components/audit-technician-details/audit-technician-details.component')
      .then(m => m.AuditTechnicianDetailsComponent)
  },
  {
    path: 'audit-search-technician-details',
    title: 'Recherche d\'un technicien',
    canActivate: [UserGuard],
    loadComponent: () => import('./audit/components/audit-search-technician-details/audit-search-technician-details.component')
      .then(m => m.AuditSearchTechnicianDetailsComponent)
  },
  {
    path: 'audit-contractor-list',
    title: 'Liste des prestataires',
    canActivate: [UserGuard],
    loadComponent: () => import('./audit/components/audit-contractor-list/audit-contractor-list.component')
      .then(m => m.AuditContractorListComponent)
  },
  {
    path: 'audit-contractor-details/:id',
    title: 'Détails d\'un prestataire',
    canActivate: [UserGuard],
    loadComponent: () => import('./audit/components/audit-contractor-details/audit-contractor-details.component')
      .then(m => m.AuditContractorDetailsComponent)
  },
  {
    path: 'loading',
    title: 'Chargement',
    loadComponent: () => import('./loading/loading.component').then(m => m.LoadingComponent)
  },
  {
    path: 'add-deposit',
    title: 'Ajout d\'un acompte',
    canActivate: [RhGuard],
    loadComponent: () => import('./deposit/components/add-deposit/add-deposit.component')
      .then(m => m.AddDepositComponent)
  },
  {
    path: 'deposit-list',
    title: 'Liste des acomptes',
    canActivate: [RhGuard],
    loadComponent: () => import('./deposit/components/deposit-list/deposit-list.component')
      .then(m => m.DepositListComponent)
  },
  {
    path: 'add-refund',
    title: 'Ajout d\'un remboursement',
    canActivate: [RhGuard],
    loadComponent: () => import('./refund/components/add-refund/add-refund.component')
      .then(m => m.AddRefundComponent)
  },
  {
    path: 'refund-list',
    title: 'Liste des remboursements',
    canActivate: [RhGuard],
    loadComponent: () => import('./refund/components/refund-list/refund-list.component')
      .then(m => m.RefundListComponent)
  },
  {
    path: 'update-refund/:id',
    title: 'Modification d\'un remboursement',
    canActivate: [RhGuard],
    loadComponent: () => import('./refund/components/update-refund/update-refund.component')
      .then(m => m.UpdateRefundComponent)
  },
  {
    path: 'add-warning',
    title: 'Ajout d\'un avertissement',
    canActivate: [RhGuard],
    loadComponent: () => import('./warning/components/add-warning/add-warning.component')
      .then(m => m.AddWarningComponent)
  },
  {
    path: 'warning-list',
    title: 'Liste des avertissements',
    canActivate: [RhGuard],
    loadComponent: () => import('./warning/components/warning-list/warning-list.component')
      .then(m => m.WarningListComponent)
  },
  {
    path: 'update-warning/:id',
    title: 'Modification d\'un avertissement',
    canActivate: [RhGuard],
    loadComponent: () => import('./warning/components/update-warning/update-warning.component')
      .then(m => m.UpdateWarningComponent)
  },
  {
    path: 'users-home',
    title: 'Gestion des utilisateurs',
    canActivate: [AdminGuard],
    loadComponent: () => import('./users/components/users-home/users-home.component')
      .then(m => m.UsersHomeComponent)
  },
  {
    path: 'users-list',
    title: 'Liste des utilisateurs',
    canActivate: [AdminGuard],
    loadComponent: () => import('./users/components/users-list/users-list.component')
      .then(m => m.UsersListComponent)
  },
  {
    path: 'add-user',
    title: 'Ajout d\'un utilisateur',
    canActivate: [AdminGuard],
    loadComponent: () => import('./users/components/add-user/add-user.component')
      .then(m => m.AddUserComponent)
  },
  {
    path: 'update-user/:id',
    title: 'Modification d\'un utilisateur',
    canActivate: [AdminGuard],
    loadComponent: () => import('./users/components/update-user/update-user.component')
      .then(m => m.UpdateUserComponent)
  },
  {
    path: 'settings/:username',
    title: 'Paramètres',
    canActivate: [AuthGuard],
    loadComponent: () => import('./users/components/settings/settings.component')
      .then(m => m.SettingsComponent)
  },
  {
    path: 'password-change/:username',
    title: 'Changement de mot de passe',
    canActivate: [AuthGuard],
    loadComponent: () => import('./users/components/password-change/password-change.component')
      .then(m => m.PasswordChangeComponent)
  },
  {
    path: 'forgot-password',
    title: 'Mot de passe oublié',
    loadComponent: () => import('./users/components/forgot-password/forgot-password.component')
      .then(m => m.ForgotPasswordComponent)
  },
  {
    path: 'not-authorized',
    title: 'Accès refusé',
    loadComponent: () => import('./shared/components/not-authorized/not-authorized.component')
      .then(m => m.NotAuthorizedComponent)
  },
  {
    path: '**',
    title: 'Page non trouvée',
    loadComponent: () => import('./shared/components/not-found-page/not-found-page.component')
      .then(m => m.NotFoundPageComponent)
  }
];
