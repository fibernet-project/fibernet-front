import {Component, OnInit} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {JsonPipe, NgForOf, NgIf} from "@angular/common";
import {ContractorService} from "../../services/contractor.service";
import {Observable} from "rxjs";
import {Contractor} from "../../classes/contractor/contractor.model";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-contractor',
  standalone: true,
  imports: [
    ButtonComponent,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    MatNativeDateModule,
    JsonPipe
  ],
  templateUrl: './add-contractor.component.html',
  styleUrl: './add-contractor.component.scss'
})
export class AddContractorComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private contractorService: ContractorService,
              private snackBar: MatSnackBar,
              private router: Router
  ) {
  }

  protected form!: FormGroup;
  protected requiredMessage: string = 'Champ obligatoire';
  protected beforeDateMessage: string = 'La date de début doit être antérieure à la date de fin.';
  protected afterDateMessage: string = 'La date de fin doit être postérieure à la date de début.';
  protected onlyNumberMessage: string = 'Veuillez saisir des chiffres uniquement';
  protected lengthTelNumberMessage: string = 'Le numéro doit avoir 10 chiffres';
  protected lengthSirenNumberMessage: string = 'Le numéro doit avoir 9 chiffres';
  protected invalidEmailMessage: string = 'Adresse email invalide';

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      responsibleName: ['', [Validators.required]],
      active: this.fb.group({
        isActive: [true]
      }),
      phoneNumber: this.fb.group( {
        phoneNumber: [
          '',
          [
            Validators.pattern(/^[0-9]+$/),
            Validators.minLength(10),
            Validators.maxLength(10),
          ],
        ],
      }),
      sirenNumber: ['', [Validators.required, Validators.minLength(9), Validators.pattern(/^[0-9]+$/)]],
      email: this.fb.group({
        email: ['', [Validators.email]]
      }),
      address: this.fb.group({
        contentAddress: ['', Validators.required],
        zipCode: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
        city: ['', Validators.required],
        country: ['', Validators.required]
      }),
      contractorInformations: this.fb.group({
        kbisStartDate: ['', [Validators.required]],
        kbisEndDate: ['', [Validators.required]],
        urssafStartDate: ['', [Validators.required]],
        urssafEndDate: ['', [Validators.required]],
        decennaleStartDate: ['', [Validators.required]],
        decennaleEndDate: ['', [Validators.required]],
        foreignWorkersStartDate: [''],
        foreignWorkersEndDate: [''],
        taxRegularityCertificateStart: [''],
        taxRegularityCertificateEnd: ['']
      },
        {
          validators: [
            this.dateRangeValidator('kbisStartDate', 'kbisEndDate'),
            this.dateRangeValidator('urssafStartDate', 'urssafEndDate'),
            this.dateRangeValidator('decennaleStartDate', 'decennaleEndDate'),
            this.dateRangeValidator('foreignWorkersStartDate', 'foreignWorkersEndDate'),
            this.dateRangeValidator('taxRegularityCertificateStart', 'taxRegularityCertificateEnd')
          ]
        })
    });
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  saveCompany(): Observable<any> {
    const companyData: Contractor = this.form.value;
    return this.contractorService.addContractor(companyData);
  }

  private dateRangeValidator(startControlName: string, endControlName: string) {
    return (group: FormGroup) => {
      const startControl = group.controls[startControlName];
      const endControl = group.controls[endControlName];

      if (startControl.value && endControl.value && startControl.value > endControl.value) {
        endControl.setErrors({ dateRangeError: true });
        startControl.setErrors({ dateRangeError: true });
      } else {
        endControl.setErrors(null);
      }
    };
  }


  onSubmit() {
    if (this.form.valid) {
      this.saveCompany().subscribe(
        () => {
          console.log("Prestataire bien ajouté");
          this.showSuccessMessage("Prestataire ajouté avec succès !");
          this.router.navigate(['/contractor-list']);
        },
        (error) => {
          console.error('Erreur lors de la requête', error);
          this.showErrorMessage("Erreur lors de l'ajout d'un prestataire.");
        }
      )
    }
  }
}
