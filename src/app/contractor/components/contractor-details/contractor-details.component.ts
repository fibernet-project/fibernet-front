import {Component, OnInit} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {DatePipe, Location, NgForOf, NgIf, NgStyle} from "@angular/common";
import {Contractor} from "../../classes/contractor/contractor.model";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {ContractorService} from "../../services/contractor.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {
  ConfirmationDialogComponent
} from "../../../shared/components/confirmation-dialog/confirmation-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {InfoDialogComponent} from "../../../shared/components/info-dialog/info-dialog.component";
import {AuthService} from "../../../auth/service/auth.service";

@Component({
  selector: 'app-contractor-details',
  standalone: true,
  imports: [
    ButtonComponent,
    DatePipe,
    NgIf,
    RouterLink,
    ConfirmationDialogComponent,
    NgStyle,
    NgForOf
  ],
  templateUrl: './contractor-details.component.html',
  styleUrl: './contractor-details.component.scss'
})
export class ContractorDetailsComponent implements OnInit {

  dateFormat: string = 'dd/MM/YYYY';
  showTechnicians: boolean = false;
  buttonText!: string;

  constructor(private route: ActivatedRoute,
              private contractorService: ContractorService,
              private snackBar: MatSnackBar,
              private router: Router,
              private dialog: MatDialog,
              private location: Location,
              protected authService: AuthService
  ) {
  }

  contractorId: string | null = null;
  contractorDetails: Contractor | undefined;

  ngOnInit(): void {
    this.contractorId = this.route.snapshot.paramMap.get('id');
    if (this.contractorId) {
      this.contractorService.getContractorById(this.contractorId).subscribe(
        (details) => {
          this.contractorDetails = details;
        },
        (error) => {
          console.error("Erreur lors de la récupération du prestataire.", error);
        }
      )
    }
  }

  toggleTechnicians() {
    this.showTechnicians = !this.showTechnicians;
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  deleteContractor() {
    if (this.contractorId) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir supprimer ce prestataire ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result && this.contractorDetails?.technicians?.length !== 0) {
          const noEmptyContractorMessage = 'Par sécurité, vous ne pouvez supprimer un prestataire ' +
            'sans avoir supprimer l\'ensemble de ses techniciens';
          const dialogRef = this.dialog.open(InfoDialogComponent, {
            width: '400px',
            height: '200px',
            data: {
              title: 'Attention',
              message: noEmptyContractorMessage,
              buttonMessage: "OK"
            }
          });
        } else if (result && this.contractorDetails?.technicians?.length === 0) {
          this.contractorService.deleteContractorById(this.contractorId!).subscribe(
            () => {
              this.showSuccessMessage('Prestataire supprimé avec succès !');
              this.router.navigate(['contractor-list']);
            },
            (error) => {
              console.error('Erreur lors de la suppression du prestataire.', error);
              this.showErrorMessage('Erreur lors de la suppression du prestataire.');
            }
          );
        }
      });
    }
  }

  goBack() {
    this.location.back();
  }

  updateStateContractor() {
    if (this.contractorId) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir modifier ce prestataire ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {

          let updatedContractor: Contractor = {

            ...this.contractorDetails!,
            active: {
              isActive: !this.contractorDetails?.active?.isActive
            }
          };

          this.contractorService.updateIsActive(updatedContractor, this.contractorId).subscribe(
            () => {

              if (this.contractorDetails) {

                this.contractorDetails.active!.isActive = !this.contractorDetails.active!.isActive;

              }
              this.showSuccessMessage('Prestataire mis à jour avec succès');
            },
            (error) => {
              console.error('Erreur lors de la mise à jour du prestataire.', error);
              this.showErrorMessage('Erreur lors de la mise à jour du prestataire');
            }
          );
        }
      });
    }
  }

  isEndDate(endDate: Date | undefined): boolean {
    if (endDate) {
      let today = new Date();
      let thirthyDaysAgo = new Date(endDate);
      thirthyDaysAgo.setDate(thirthyDaysAgo.getDate() - 30);

      return today.getTime() >= thirthyDaysAgo.getTime();
    }

    return false;
  }

}
