import { Component } from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-contractor-home',
  standalone: true,
  imports: [
    ButtonComponent,
    RouterLink
  ],
  templateUrl: './contractor-home.component.html',
  styleUrl: './contractor-home.component.scss'
})
export class ContractorHomeComponent {

}
