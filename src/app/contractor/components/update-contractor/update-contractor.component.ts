import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {JsonPipe, Location, NgForOf, NgIf} from "@angular/common";
import {ContractorService} from "../../services/contractor.service";
import {Contractor} from "../../classes/contractor/contractor.model";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatSelectModule} from "@angular/material/select";
import {MatButtonModule} from "@angular/material/button";
import {MatNativeDateModule} from "@angular/material/core";
import {ButtonComponent} from "../../../shared/components/button/button.component";

@Component({
  selector: 'app-update-contractor',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    NgIf,
    MatDatepickerModule,
    MatSelectModule,
    MatButtonModule,
    MatNativeDateModule,
    JsonPipe,
    ButtonComponent,
    NgForOf
  ],
  templateUrl: './update-contractor.component.html',
  styleUrl: './update-contractor.component.scss'
})
export class UpdateContractorComponent implements OnInit {

  contractorId: string | null = null;
  contractorDetails: Contractor | undefined;
  contractorForm!: FormGroup;
  protected requiredMessage: string = 'Champ obligatoire';
  protected onlyNumberMessage: string = 'Veuillez saisir des chiffres uniquement';
  protected lengthTelNumberMessage: string = 'Le numéro doit avoir 10 chiffres';
  protected beforeDateMessage: string = 'La date de début doit être antérieure à la date de fin.';
  protected afterDateMessage: string = 'La date de fin doit être postérieure à la date de début.';
  protected lengthSirenNumberMessage: string = 'Le numéro doit avoir 9 chiffres';
  protected invalidEmailMessage: string = 'Adresse email invalide';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private contractorService: ContractorService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              private location: Location) {
    this.contractorForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      responsibleName: ['', [Validators.required]],
      address: this.formBuilder.group({
        contentAddress: ['', Validators.required],
        zipCode: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
        city: ['', Validators.required],
        country: ['', Validators.required],
      }),
      contractorInformations: this.formBuilder.group({
        kbisStartDate: ['', [Validators.required]],
        kbisEndDate: ['', [Validators.required]],
        urssafStartDate: ['', [Validators.required]],
        urssafEndDate: ['', [Validators.required]],
        decennaleStartDate: ['', [Validators.required]],
        decennaleEndDate: ['', [Validators.required]],
        foreignWorkersStartDate: [''],
        foreignWorkersEndDate: [''],
        taxRegularityCertificateStart: [''],
        taxRegularityCertificateEnd: [''],
      }),
      phoneNumber: this.formBuilder.group({
        phoneNumber: [
          '',
          [
            Validators.pattern(/^[0-9]+$/),
            Validators.minLength(10),
            Validators.maxLength(10),
          ],
        ]
      }),
      sirenNumber: ['', [Validators.required, Validators.minLength(9), Validators.pattern(/^[0-9]+$/)]],
      email: this.formBuilder.group({
        email: ['', [Validators.email]]
      })
    });
  }

  ngOnInit(): void {
    this.contractorId = this.route.snapshot.paramMap.get('id');
    if (this.contractorId) {
      this.contractorService.getContractorById(this.contractorId).subscribe(
        (details) => {
          this.contractorDetails = details;

          // Conversion from timestamp format to Date format
          const kbisStartDate = details.contractorInformations?.kbisStartDate ?
            new Date(details.contractorInformations.kbisStartDate) : null;
          const kbisEndDate = details.contractorInformations?.kbisEndDate ?
            new Date(details.contractorInformations.kbisEndDate) : null;
          const urssafStartDate = details.contractorInformations?.urssafStartDate ?
            new Date(details.contractorInformations.urssafStartDate) : null;
          const urssafEndDate = details.contractorInformations?.urssafEndDate ?
            new Date(details.contractorInformations.urssafEndDate) : null;
          const decennaleStartDate = details.contractorInformations?.decennaleStartDate ?
            new Date(details.contractorInformations.decennaleStartDate) : null;
          const decennaleEndDate = details.contractorInformations?.decennaleEndDate ?
            new Date(details.contractorInformations.decennaleEndDate) : null;
          const foreignWorkersStartDate = details.contractorInformations?.foreignWorkersStartDate ?
            new Date(details.contractorInformations.foreignWorkersStartDate) : null;
          const foreignWorkersEndDate = details.contractorInformations?.foreignWorkersEndDate ?
            new Date(details.contractorInformations.foreignWorkersEndDate) : null;
          const taxRegularityCertificateStart = details.contractorInformations?.taxRegularityCertificateStart ?
            new Date(details.contractorInformations.taxRegularityCertificateStart) : null;
          const taxRegularityCertificateEnd = details.contractorInformations?.taxRegularityCertificateEnd ?
            new Date(details.contractorInformations.taxRegularityCertificateEnd) : null;

          this.contractorForm.patchValue({
            name: details.name,
            responsibleName: details.responsibleName,
            address: {
              contentAddress: details.address?.contentAddress,
              zipCode: details.address?.zipCode,
              city: details.address?.city,
              country: details.address?.country
            },
            contractorInformations: {
              kbisStartDate: kbisStartDate,
              kbisEndDate: kbisEndDate,
              urssafStartDate: urssafStartDate,
              urssafEndDate: urssafEndDate,
              decennaleStartDate: decennaleStartDate,
              decennaleEndDate: decennaleEndDate,
              foreignWorkersStartDate: foreignWorkersStartDate,
              foreignWorkersEndDate: foreignWorkersEndDate,
              taxRegularityCertificateStart: taxRegularityCertificateStart,
              taxRegularityCertificateEnd: taxRegularityCertificateEnd
            },
            phoneNumber: {
              phoneNumber: details.phoneNumber?.phoneNumber
            },
            sirenNumber: details.sirenNumber,
            email: {
              email: details.email?.email
            }
          });
        },
        (error) => {
          console.error('Erreur lors de la récupération des détails du prestataire.', error);
        }
      );
    } else {
      console.error('ID du prestataire non trouvé dans l\'URL.');
    }
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  protected goBack(): void {
    this.location.back();
  }

  onSubmit() {
    if (this.contractorForm.valid) {
      let formValue = this.contractorForm.value;

      let updatedContractor: Contractor = {
        name: formValue.name,
        responsibleName: formValue.responsibleName,
        address: {
          contentAddress: formValue.address.contentAddress,
          zipCode: formValue.address.zipCode,
          city: formValue.address.city,
          country: formValue.address.country
        },
        contractorInformations: {
          kbisStartDate: formValue.contractorInformations.kbisStartDate,
          kbisEndDate: formValue.contractorInformations.kbisEndDate,
          urssafStartDate: formValue.contractorInformations.urssafStartDate,
          urssafEndDate: formValue.contractorInformations.urssafEndDate,
          decennaleStartDate: formValue.contractorInformations.decennaleStartDate,
          decennaleEndDate: formValue.contractorInformations.decennaleEndDate,
          foreignWorkersStartDate: formValue.contractorInformations.foreignWorkersStartDate,
          foreignWorkersEndDate: formValue.contractorInformations.foreignWorkersEndDate,
          taxRegularityCertificateStart: formValue.contractorInformations.taxRegularityCertificateStart,
          taxRegularityCertificateEnd: formValue.contractorInformations.taxRegularityCertificateEnd
        },
        phoneNumber: {
          phoneNumber: formValue.phoneNumber.phoneNumber
        },
        sirenNumber: formValue.sirenNumber,
        email: {
          email: formValue.email.email
        }
      };

      this.contractorService.updateContractor(updatedContractor, this.contractorId).subscribe(
        () => {
          this.showSuccessMessage('Prestataire mis à jour avec succès');
          this.router.navigate(['/contractor-list']);
        },
        (error) => {
          console.error('Erreur lors de la mise à jour du prestataire.', error);
          this.showErrorMessage("Erreur lors de la modification du prestataire");
        }
      );
    }
  }

}
