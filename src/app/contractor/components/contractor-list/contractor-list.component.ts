import {Component, OnInit} from '@angular/core';
import {NgForOf, NgIf, NgStyle} from "@angular/common";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {Contractor} from "../../classes/contractor/contractor.model";
import {ContractorService} from "../../services/contractor.service";
import {RouterLink} from "@angular/router";
import {ContractorInformations} from "../../classes/contractor-informations/contractor-informations.model";
import {MatIconModule} from "@angular/material/icon";

@Component({
  selector: 'app-contractor-list',
  standalone: true,
  imports: [
    NgIf,
    ButtonComponent,
    NgForOf,
    RouterLink,
    NgStyle,
    MatIconModule
  ],
  templateUrl: './contractor-list.component.html',
  styleUrl: './contractor-list.component.scss'
})
export class ContractorListComponent implements OnInit {

  constructor(private contractorService: ContractorService) {
  }

  contractors: Contractor[] = [];

  ngOnInit(): void {
    this.getAllContractors();
  }

  getAllContractors() {
    this.contractorService.getAllContractors().subscribe(
      (data: Contractor[]) => {
        this.contractors = data;
      },
      (error) => {
        console.error('Erreur lors de la récupération des prestataires', error);
      }
    );
  }

  isEndDate(endDate: Date | undefined): boolean {
    if (endDate) {
      let today = new Date();
      let thirtyDaysAgo = new Date(endDate);
      thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);

      return today.getTime() >= thirtyDaysAgo.getTime();
    }
    return false;
  }

  showWarningIcon(contractorInformations: ContractorInformations | undefined): boolean {
    return (
      this.isEndDate(contractorInformations?.kbisEndDate) ||
      this.isEndDate(contractorInformations?.urssafEndDate) ||
      this.isEndDate(contractorInformations?.decennaleEndDate) ||
      this.isEndDate(contractorInformations?.foreignWorkersEndDate) ||
      this.isEndDate(contractorInformations?.taxRegularityCertificateEnd)
    );
  }

}
