import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Contractor} from "../classes/contractor/contractor.model";
import {environment} from "../../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class ContractorService {
  private apiUrl = environment.apiUrl;
  private endPoint: string = '/contractors';

  constructor(private httpClient: HttpClient) {
  }

  public getAllContractors(): Observable<Contractor[]> {
    return this.httpClient.get<Contractor[]>(`${this.apiUrl + this.endPoint}`);
  }

  public addContractor(contractor: Contractor): Observable<Object> {
    return this.httpClient.post(`${this.apiUrl + this.endPoint}`, contractor);
  }

  public getContractorById(id: string): Observable<Contractor> {
    return this.httpClient.get<Contractor>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  public deleteContractorById(id: string): Observable<void> {
    return this.httpClient.delete<void>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  public updateContractor(contractor: Contractor, id: string | null): Observable<Object> {
    if (id === null) {
      console.error('Le prestataire doit avoir un ID pour être mis à jour.');
      return throwError('Le prestataire doit avoir un ID pour être mis à jour.');
    }

    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}`, contractor);
  }

  public updateIsActive(contractor: Contractor, id: string | null): Observable<Object> {
    if (id === null) {
      console.error('Le prestataire doit avoir un ID pour être mis à jour.');
      return throwError('Le prestataire doit avoir un ID pour être mis à jour.');
    }

    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}/isActive`, contractor);
  }
}
