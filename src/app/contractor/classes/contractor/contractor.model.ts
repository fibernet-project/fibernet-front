import {Address} from "../../../shared/classes/address/address.model";
import {ContractorInformations} from "../contractor-informations/contractor-informations.model";
import {Technician} from "../../../technician/classes/technician/technician.model";
import {Active} from "../../../shared/classes/active/active.model";
import {BaseEntity} from "../../../shared/classes/bases/base-entity.model";
import {Email} from "../../../shared/classes/email/email.model";
import {PhoneNumber} from "../../../shared/classes/phoneNumber/phone-number.model";

export class Contractor extends BaseEntity {
  name?: string;
  responsibleName?: string;
  technicians?: Technician[];
  address?: Address;
  contractorInformations?: ContractorInformations;
  sirenNumber?: number;
  active?: Active;
  phoneNumber?: PhoneNumber;
  email?: Email;
}
