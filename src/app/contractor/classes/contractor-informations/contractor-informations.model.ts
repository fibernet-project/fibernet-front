export class ContractorInformations {

  kbisStartDate?: Date;
  kbisEndDate?: Date;
  urssafStartDate?: Date;
  urssafEndDate?: Date;
  decennaleStartDate?: Date;
  decennaleEndDate?: Date;

  // Certificate for foreign workers
  foreignWorkersStartDate?: Date;
  foreignWorkersEndDate?: Date;
  taxRegularityCertificateStart?: Date;
  taxRegularityCertificateEnd?: Date;
}
