import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Refund} from "../classes/refund.model";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RefundService {

  private apiUrl = environment.apiUrl;
  private endPoint : string = '/refunds';

  constructor(private httpClient: HttpClient) { }

  public addRefund(refund: Refund): Observable<Object> {
    return this.httpClient.post(`${this.apiUrl + this.endPoint}`, refund);
  }

  public getAllRefunds(): Observable<Refund[]> {
    return this.httpClient.get<Refund[]>(`${this.apiUrl + this.endPoint}`);
  }

  public deleteRefundById(id: number) {
    return this.httpClient.delete<void>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  public getRefundById(id: string | undefined): Observable<Refund> {
    return this.httpClient.get<Refund>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  updateRefundState(refund: Refund, id: string | null): Observable<Object> {

    if (id === null) {
      console.error("Le remboursement doit avoir un ID pour être mis à jour.");
      return throwError("Le remboursement doit avoir un ID pour être mis à jour.");
    }

    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}`, refund);
  }
}
