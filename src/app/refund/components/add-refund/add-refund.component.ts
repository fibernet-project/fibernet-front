import {Component, OnInit} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {NgForOf, NgIf} from "@angular/common";
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {Employee} from "../../../employee/classes/employee/employee.model";
import {EmployeeService} from "../../../employee/services/employee.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {MatRadioModule} from "@angular/material/radio";
import {RefundService} from "../../services/refund.service";
import {Refund} from "../../classes/refund.model";

@Component({
  selector: 'app-add-refund',
  standalone: true,
  imports: [
    ButtonComponent,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatRadioModule,
    MatSelectModule,
    NgIf,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgForOf
  ],
  templateUrl: './add-refund.component.html',
  styleUrl: './add-refund.component.scss'
})
export class AddRefundComponent implements OnInit {
  form!: FormGroup;
  employees: Employee[] = [];
  protected requiredMessage: string = 'Champ obligatoire';
  protected onlyNumberMessage: string = 'Veuillez saisir des chiffres uniquement';
  protected emptyEmployeeMessage: string = 'Aucun employé à afficher. Veuillez aller dans le menu "Employés".';

  constructor(private formBuilder: FormBuilder,
              private employeeService: EmployeeService,
              private snackBar: MatSnackBar,
              private router: Router,
              private refundService: RefundService) {
  }

  ngOnInit() {
    this.initForm();
    this.getAllEmployees();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      date: ['', [Validators.required]],
      totalRefund: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      employee: ['', [Validators.required]],
      refundState: ['', [Validators.required]]
    })
  }

  getAllEmployees() {
    this.employeeService.getAllEmployees().subscribe(
      (data) => {
        this.employees = data;
      },
      (error) => {
        console.error('Erreur lors de la récupération des données', error);
      });
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  saveRefund(): Observable<any> {
    const refundData: Refund = this.form.value;
    return this.refundService.addRefund(refundData);
  }

  onSubmit() {
    if (this.form.valid) {
      this.saveRefund().subscribe(
        (response) => {
          console.log('Réponse du serveur : ', response);
          this.showSuccessMessage('Remboursement ajouté avec succès !');
          this.router.navigate(['refund-list']);
        },
        (error) => {
          console.error('Erreur lors de la requête', error);
          this.showErrorMessage('Erreur lors de l\'ajout du remboursement.');
        }
      );
    }
  }
}
