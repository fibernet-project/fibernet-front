import {Component, OnInit} from '@angular/core';
import {Refund} from "../../classes/refund.model";
import {RefundService} from "../../services/refund.service";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {DatePipe} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {RouterLink} from "@angular/router";
import {
  ConfirmationDialogComponent
} from "../../../shared/components/confirmation-dialog/confirmation-dialog.component";
import {displayRefundStateValue} from "../../../shared/enums/RefundState.enum";
import {MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {ReactiveFormsModule} from "@angular/forms";

@Component({
  selector: 'app-refund-list',
  standalone: true,
  imports: [
    ButtonComponent,
    DatePipe,
    MatIconModule,
    RouterLink,
    MatOptionModule,
    MatSelectModule,
    ReactiveFormsModule
  ],
  templateUrl: './refund-list.component.html',
  styleUrl: './refund-list.component.scss'
})
export class RefundListComponent implements OnInit {

  refunds: Refund[] = [];
  dateFormat: string = 'dd/MM/YYYY';

  constructor(private refundService: RefundService,
              private dialog: MatDialog,
              private snackBar: MatSnackBar) {
  }
  ngOnInit(): void {
    this.getRefunds();
  }

  getRefunds() {
    this.refundService.getAllRefunds().subscribe(
      (data) => {
        this.refunds = data;
      },
      (error) => {
        console.error("Erreur lors de la récupération des remboursements.", error);
      }
    )
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  deleteRefund(id: number | undefined) {
    if (id) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir supprimer ce remboursement ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.refundService.deleteRefundById(id!).subscribe(
            () => {
              this.showSuccessMessage('Remboursement supprimé avec succès');
              this.getRefunds();
            },
            (error) => {
              console.error('Erreur lors de la suppression du remboursement.', error);
              this.showErrorMessage('Erreur lors de la suppression du remboursement.');
            }
          );
        }
      });
    }
  }


  protected readonly displayRefundStateValue = displayRefundStateValue;
}
