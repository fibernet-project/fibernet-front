import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRefundComponent } from './update-refund.component';

describe('UpdateRefundComponent', () => {
  let component: UpdateRefundComponent;
  let fixture: ComponentFixture<UpdateRefundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UpdateRefundComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UpdateRefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
