import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {DatePipe, Location, NgIf} from "@angular/common";
import {Refund} from "../../classes/refund.model";
import {RefundService} from "../../services/refund.service";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {ButtonComponent} from "../../../shared/components/button/button.component";

@Component({
  selector: 'app-update-refund',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    NgIf,
    DatePipe,
    ButtonComponent
  ],
  templateUrl: './update-refund.component.html',
  styleUrl: './update-refund.component.scss'
})
export class UpdateRefundComponent implements OnInit {

  refundId: string | null = null;
  refundDetails: Refund | undefined;
  refundForm!: FormGroup;
  protected requiredMessage: string = 'Champ obligatoire';
  dateFormat: string = 'dd/MM/YYYY';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              private location: Location,
              private refundService: RefundService) {
    this.refundForm = this.formBuilder.group({
      refundState: ['', [Validators.required]]
    })
  }

  ngOnInit() {

    this.refundId = this.route.snapshot.paramMap.get('id');
    if (this.refundId) {
      this.refundService.getRefundById(this.refundId).subscribe(
        (details) => {
          this.refundDetails = details;
          this.initializeForm(details);
        },
        (error) => {
          console.error('Erreur lors de la récupération des détails du remboursement.', error);
        }
      );
    } else {
      console.error('ID du remboursement non trouvé dans l\'URL.');
    }
  }

  private initializeForm(details: Refund) {
    this.refundForm.patchValue({
      refundState: details.refundState
    });
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  protected goBack(): void {
    this.location.back();
  }

  onSubmit() {
    if (this.refundForm.valid) {
      let formValue = this.refundForm.value;

      let updatedRefund: Refund = {
        refundState: formValue.refundState
      };

      this.refundService.updateRefundState(updatedRefund, this.refundId).subscribe(
        () => {
          this.showSuccessMessage('État du remboursement mis à jour.');
          this.router.navigate(['refund-list']);
        },
        (error) => {
          console.error('Erreur lors de la mise à jour du remboursement.', error);
          this.showErrorMessage("Erreur lors de la modification du remboursement.");
        }
      )
    }
  }
}
