import {RefundState} from "../../shared/enums/RefundState.enum";
import {BaseEntity} from "../../shared/classes/bases/base-entity.model";
import {Employee} from "../../employee/classes/employee/employee.model";
import {Operations} from "../../shared/classes/operations/operations.model";

export class Refund extends Operations {

  totalRefund?: number;
  refundState?: RefundState;

  constructor(data: Partial<Employee>) {
    super(data);
    Object.assign(this, data);
  }
}
