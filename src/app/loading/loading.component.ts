import { Component } from '@angular/core';
import {TitleComponent} from "../shared/components/title/title.component";

@Component({
  selector: 'app-loading',
  standalone: true,
  imports: [
    TitleComponent
  ],
  templateUrl: './loading.component.html',
  styleUrl: './loading.component.scss'
})
export class LoadingComponent {

}
