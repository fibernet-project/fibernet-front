import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {Location, NgIf} from "@angular/common";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {UsersService} from "../../services/users.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-password-change',
  standalone: true,
  imports: [
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    NgIf,
    ReactiveFormsModule,
    ButtonComponent
  ],
  templateUrl: './password-change.component.html',
  styleUrl: './password-change.component.scss'
})
export class PasswordChangeComponent implements OnInit {

  form!: FormGroup;
  requiredMessage: string = 'Champ obligatoire';
  mismatchPasswords = 'Le mot de passe et la confirmation ne sont pas identiques';
  hidePassword: boolean = true;
  hideConfirmPassword: boolean = true;
  errorMessage: string = 'Le mot de passe doit contenir au moins une minuscule, une majuscule, un caractère spécial et un chiffre';
  minLengthMessage: string = 'Ce champ doit contenir au moins 8 caractères';
  username: string | null = null;
  constructor(private formBuilder: FormBuilder,
              private location: Location,
              private userService: UsersService,
              private router: Router,
              private snackBar: MatSnackBar,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.initForm();
    this.username = this.route.snapshot.paramMap.get('username');
  }

  initForm() {
  this.form = this.formBuilder.group({
    password: ['', [Validators.required, Validators.minLength(8),
      Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/),]],
    confirmPassword: ['', Validators.required]
  });

    this.form.get('password')?.valueChanges.subscribe(() => this.validatePasswordConfirmation());
    this.form.get('confirmPassword')?.valueChanges.subscribe(() => this.validatePasswordConfirmation());
  }

  private validatePasswordConfirmation() {
    const password = this.form.get('password')?.value;
    const confirmPassword = this.form.get('confirmPassword')?.value;
    const passwordMatch = password === confirmPassword;
    this.form.get('confirmPassword')?.setErrors(passwordMatch ? null : {mismatch: true});
  }

  togglePasswordVisibility(): void {
    this.hidePassword = !this.hidePassword;
  }

  toggleConfirmPasswordVisibility() {
    this.hideConfirmPassword = !this.hideConfirmPassword;
  }

  goBack() {
    this.location.back();
  }

  onSubmit() {
    const confirmPassword = this.form.get('password')?.value;

    this.userService.updatePassword(confirmPassword, this.username).subscribe(
      (data) => {
        this.snackBar.open('Mot de passe modifié avec succès', 'Fermer', {duration: 3000});
        this.router.navigate(['/settings', this.username]);
      },
      error => {
        console.error('Erreur lors de la modification du mot de passe.', error);
        this.snackBar.open('Erreur lors de la modification du mot de passe.', 'Fermer', {duration: 3000});
      }
    );

  }
}
