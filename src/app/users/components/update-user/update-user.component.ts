import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Location, NgForOf, NgIf} from '@angular/common';
import { UsersService } from '../../services/users.service';
import { AppUser } from '../../classes/user/app-user.model';
import { Roles } from '../../../shared/enums/Roles.enum';
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {ButtonComponent} from "../../../shared/components/button/button.component";

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    NgIf,
    MatSelectModule,
    ButtonComponent,
    NgForOf
  ],
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {

  form!: FormGroup;
  roles = Object.values(Roles);
  requiredMessage: string = 'Champ obligatoire';
  mismatchPasswords = 'Le mot de passe et la confirmation ne sont pas identiques';
  userId: string | null = null;
  userDetails: AppUser | undefined;

  constructor(
    private router: Router,
    private userService: UsersService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private location: Location,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.userId = this.route.snapshot.paramMap.get('id');
    if (this.userId) {
      this.userService.getUserById(this.userId).subscribe(
        (details) => {
          this.userDetails = details;
          this.form.patchValue({
            lastName: details.lastName,
            firstName: details.firstName,
            email: { email: details.email?.email },
            username: details.username,
            roles: details.roles
          });
        },
        (error) => {
          console.error('Erreur lors de la récupération des détails de l\'utilisateur', error);
        }
      )
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      email: this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]]
      }),
      username: ['', [Validators.required]],
      roles: [[], Validators.required]
    });

    this.form.get('lastName')?.valueChanges.subscribe(() => this.updateUsername());
    this.form.get('firstName')?.valueChanges.subscribe(() => this.updateUsername());
  }

  private updateUsername() {
    const lastName = this.form.get('lastName')?.value;
    const firstName = this.form.get('firstName')?.value;
    const username = `${firstName.charAt(0)}${lastName}`.toLowerCase();
    this.form.patchValue({ username });
  }

  protected goBack(): void {
    this.location.back();
  }

  onSubmit() {
    if (this.form.valid) {
    let formValue = this.form.value;
    let updatedUser: AppUser = {
      lastName: formValue.lastName,
      firstName: formValue.firstName,
      username: formValue.username,
      email: {
        email: formValue.email.email
      },
      roles: formValue.roles
    }
      this.userService.updateUser(updatedUser, this.userId).subscribe(
        () => {
          this.snackBar.open('Utilisateur mis à jour avec succès', 'Fermer', { duration: 3000 });
          this.router.navigateByUrl('/users-list');
        },
        error => {
          console.error('Erreur lors de la mise à jour de l\'utilisateur :', error);
          this.snackBar.open('Erreur lors de la mise à jour de l\'utilisateur', 'Fermer', { duration: 3000 });
        }
      );
    }
  }
}
