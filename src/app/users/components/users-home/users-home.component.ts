import { Component } from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-users-home',
  standalone: true,
  imports: [
    ButtonComponent,
    RouterLink
  ],
  templateUrl: './users-home.component.html',
  styleUrl: './users-home.component.scss'
})
export class UsersHomeComponent {

}
