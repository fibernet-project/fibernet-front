import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {Location, NgIf} from "@angular/common";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {UsersService} from "../../services/users.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-forgot-password',
  standalone: true,
  imports: [
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    NgIf,
    ReactiveFormsModule,
    ButtonComponent
  ],
  templateUrl: './forgot-password.component.html',
  styleUrl: './forgot-password.component.scss'
})
export class ForgotPasswordComponent implements OnInit{

  form!: FormGroup;
  requiredMessage: string = 'Champ obligatoire';

  constructor(private formBuilder: FormBuilder,
              private userService: UsersService,
              private router: Router,
              private snackBar: MatSnackBar,
              private location: Location) {
  }
  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required]
    })
  }

  goBack() {
    this.location.back();
  }

  onSubmit() {
    let username = this.form.get('username')?.value;
    this.userService.forgotPassword(username).subscribe(
      (data) => {
        console.log("Le mot de passe a été envoyé sur votre adresse mail.", data);
        this.snackBar.open('Le mot de passe a été envoyé sur votre adresse mail.', 'Fermer', {duration: 3000});
        this.router.navigateByUrl('/login');
      },
      (error) => {
        console.error("Erreur lors de la requête", error);
        this.snackBar.open('Erreur lors de la requête', 'Fermer', {duration: 3000});
      }
    )
  }

}
