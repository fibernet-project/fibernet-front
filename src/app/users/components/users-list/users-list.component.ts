import {Component, OnInit, ViewChild} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {NgForOf, NgIf} from "@angular/common";
import {PaginatorComponent} from "../../../shared/components/paginator/paginator.component";
import {RouterLink} from "@angular/router";
import {AppUser} from "../../classes/user/app-user.model";
import {UsersService} from "../../services/users.service";
import {MatIconModule} from "@angular/material/icon";
import {
  ConfirmationDialogComponent
} from "../../../shared/components/confirmation-dialog/confirmation-dialog.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-users-list',
  standalone: true,
  imports: [
    ButtonComponent,
    NgForOf,
    NgIf,
    RouterLink,
    MatIconModule
  ],
  templateUrl: './users-list.component.html',
  styleUrl: './users-list.component.scss'
})
export class UsersListComponent implements OnInit {
  @ViewChild(PaginatorComponent) paginator: PaginatorComponent | undefined;

  users: AppUser[] = [];

  constructor(private userService: UsersService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {}

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(): void {
    this.userService.getAllUsers().subscribe(
      (data: AppUser[]) => {
        this.users = data;
      },
      (error) => {
        console.error('Error fetching users:', error);
      }
    );
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  deleteUser(id: number | undefined) {
    if (id) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir supprimer cet utilisateur ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.userService.deleteUserById(id).subscribe(
            () => {
              this.showSuccessMessage('Utilisateur supprimé avec succès');
              this.getUsers();
            },
            (error) => {
              console.error('Erreur lors de la suppression de l\'utilisateur.', error);
              this.showErrorMessage('Erreur lors de la suppression de l\'utilisateur.');
            }
          );
        }
      });
    }
  }

}

