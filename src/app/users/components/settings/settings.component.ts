import {Component, OnInit} from '@angular/core';
import {UsersService} from "../../services/users.service";
import {ActivatedRoute, RouterLink} from "@angular/router";
import {AppUser} from "../../classes/user/app-user.model";
import {NgIf} from "@angular/common";
import {ButtonComponent} from "../../../shared/components/button/button.component";

@Component({
  selector: 'app-settings',
  standalone: true,
  imports: [
    NgIf,
    ButtonComponent,
    RouterLink
  ],
  templateUrl: './settings.component.html',
  styleUrl: './settings.component.scss'
})
export class SettingsComponent implements OnInit {

  username: string | null = null;
  userDetails: AppUser | undefined;
  constructor(private userService: UsersService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.username = this.route.snapshot.paramMap.get('username');
    if (this.username) {
      this.userService.getUserByUsername(this.username).subscribe(
        (data) => {
            this.userDetails = data;
        },
        (error) => {
          console.error('Erreur lors de la récupération des détails de l\'utilisateur.', error);
        }
      )
    }
  }
}
