import {Component, OnInit} from '@angular/core';
import {MatInputModule} from "@angular/material/input";
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UsersService} from "../../services/users.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatSelectModule} from "@angular/material/select";
import {NgForOf, NgIf} from "@angular/common";
import {MatButtonModule} from "@angular/material/button";
import {Roles} from "../../../shared/enums/Roles.enum";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {MatIconModule} from "@angular/material/icon";

@Component({
  selector: 'app-add-user',
  standalone: true,
  imports: [
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    NgForOf,
    MatButtonModule,
    ButtonComponent,
    NgIf,
    MatIconModule
  ],
  templateUrl: './add-user.component.html',
  styleUrl: './add-user.component.scss'
})
export class AddUserComponent implements OnInit {

  form!: FormGroup;
  roles = Object.values(Roles);
  requiredMessage: string = 'Champ obligatoire';
  mismatchPasswords = 'Le mot de passe et la confirmation ne sont pas identiques';
  hidePassword: boolean = true;
  errorMessage: string = 'Le mot de passe doit contenir au moins une minuscule, une majuscule, un caractère spécial et un chiffre';
  minLengthMessage: string = 'Ce champ doit contenir au moins 8 caractères';


  constructor(
    private router: Router,
    private userService: UsersService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      email: this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]]
      }),
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8),
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/),]],
      confirmPassword: ['', Validators.required],
      roles: [[''], Validators.required]
    });

    this.form.get('lastName')?.valueChanges.subscribe(() => this.updateUsername());
    this.form.get('firstName')?.valueChanges.subscribe(() => this.updateUsername());

    this.form.get('password')?.valueChanges.subscribe(() => this.validatePasswordConfirmation());
    this.form.get('confirmPassword')?.valueChanges.subscribe(() => this.validatePasswordConfirmation());
  }

  private updateUsername() {
    const lastName = this.form.get('lastName')?.value;
    const firstName = this.form.get('firstName')?.value;
    const username = `${firstName.charAt(0)}${lastName}`.toLowerCase();
    this.form.patchValue({username});
  }

  private validatePasswordConfirmation() {
    const password = this.form.get('password')?.value;
    const confirmPassword = this.form.get('confirmPassword')?.value;
    const passwordMatch = password === confirmPassword;
    this.form.get('confirmPassword')?.setErrors(passwordMatch ? null : {mismatch: true});
  }

  togglePasswordVisibility(): void {
    this.hidePassword = !this.hidePassword;
  }

  onSubmit() {
    const {confirmPassword, ...userData} = this.form.value;

    this.userService.addUser(userData).subscribe(
      () => {
        this.snackBar.open('Utilisateur ajouté avec succès', 'Fermer', {duration: 3000});
        this.router.navigateByUrl('/users-list');
      },
      error => {
          console.error('Erreur lors de l\'ajout de l\'utilisateur :', error);
          this.snackBar.open('Erreur lors de l\'ajout de l\'utilisateur', 'Fermer', {duration: 3000});
      }
    );
  }



}
