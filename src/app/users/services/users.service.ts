import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {AppUser} from "../classes/user/app-user.model";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private apiUrl = environment.apiUrl;
  private endPoint: string = '/users';
  private errorIdMessage: string = "L'utilisateur doit avoir un ID pour être mis à jour.";
  private errorUsernameMessage: string = "L'utilisateur doit avoir un nom d'utilisateur pour être mis à jour.";

  constructor(private httpClient: HttpClient) {
  }

  public getAllUsers(): Observable<AppUser[]> {
    return this.httpClient.get<AppUser[]>(`${this.apiUrl + this.endPoint}`);
  }

  public addUser(user: AppUser): Observable<Object> {
    return this.httpClient.post(`${this.apiUrl + this.endPoint}/create`, user);
  }

  deleteUserById(id: number): Observable<void> {
    return this.httpClient.delete<void>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  public getUserById(id: string) : Observable<AppUser> {
    return this.httpClient.get<AppUser>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  public getUserByUsername(username: string) : Observable<AppUser> {
    return this.httpClient.get<AppUser>(`${this.apiUrl + this.endPoint}/api/${username}`);
  }

  public updateUser(user: AppUser, id: string | null): Observable<Object> {

    if (id === null) {
      console.error(this.errorIdMessage);
      return throwError(this.errorIdMessage);
    }
    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}`, user);
  }

  public updatePassword(password: string, username: string | null){
    if (username === null) {
      console.error(this.errorUsernameMessage);
      return throwError(this.errorUsernameMessage);
    }
    return this.httpClient.put(`${this.apiUrl + this.endPoint}/change/${username}`, password);
  }

  forgotPassword(username: string) {
    return this.httpClient.post(`${this.apiUrl}/users/forgot-password?username=${username}`, null, { responseType: 'text' });
  }
}
