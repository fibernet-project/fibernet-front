import {Roles} from "../../../shared/enums/Roles.enum";
import {Person} from "../../../shared/classes/person/person.model";

export class AppUser extends Person {
  username?: string;
  password?: string;
  roles?: Roles[];
}
