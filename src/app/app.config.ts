import {ApplicationConfig} from '@angular/core';
import {provideRouter} from '@angular/router';

import {routes} from './app.routes';
import {provideAnimations} from '@angular/platform-browser/animations';
import {MAT_DATE_LOCALE} from "@angular/material/core";
import {HTTP_INTERCEPTORS, provideHttpClient, withInterceptors} from "@angular/common/http";
import {DatePipe} from "@angular/common";
import {appHttpInterceptor} from "./interceptors/app-http.interceptor";

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes), provideAnimations(),
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    provideHttpClient(
      withInterceptors([appHttpInterceptor])
    ),
    DatePipe,
  ]
};
