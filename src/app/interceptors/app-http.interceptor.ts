import { HttpInterceptorFn } from '@angular/common/http';
import {catchError, throwError} from 'rxjs';
import { AuthService } from "../auth/service/auth.service";
import {inject} from "@angular/core";

export const appHttpInterceptor: HttpInterceptorFn =
  (request, next) => {

    const authService = inject(AuthService);

    if (!request.url.includes("/auth/login") && !request.url.includes("/forgot-password")) {
      const newRequest = request.clone({
        setHeaders: {
          'Authorization': 'Bearer ' + authService.accessToken
        }
      });

      return next(newRequest).pipe(
        catchError(err => {
          if (err.status == (401 || 403)) {
            authService.logout();
          }
          return throwError(err.message);
        })
      );
    } else {
      return next(request);
    }
  };
