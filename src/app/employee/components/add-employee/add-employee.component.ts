import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {EmployeeService} from '../../services/employee.service';
import {Employee} from '../../classes/employee/employee.model';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {JsonPipe, NgForOf, NgIf} from "@angular/common";
import {MatNativeDateModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Observable} from "rxjs";
import {MatRadioModule} from "@angular/material/radio";

@Component({
  selector: 'app-add-employee',
  standalone: true,
  templateUrl: './add-employee.component.html',
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    JsonPipe,
    ButtonComponent,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    NgIf,
    NgForOf,
    MatRadioModule
  ],
  styleUrls: ['./add-employee.component.scss']
})

export class AddEmployeeComponent implements OnInit {
  form!: FormGroup;
  protected requiredMessage: string = 'Champ obligatoire';
  protected onlyNumberMessage: string = 'Veuillez saisir des chiffres uniquement';
  protected lengthSecuNumberMessage: string = 'Le numéro doit avoir 13 chiffres';
  protected lengthTelNumberMessage: string = 'Le numéro doit avoir 10 chiffres';
  protected invalidEmailMessage: string = 'Adresse email invalide';
  protected beforeDateMessage: string = 'La date de début doit être antérieure à la date de fin.';
  protected afterDateMessage: string = 'La date de fin doit être postérieure à la date de début.';

  constructor(private formBuilder: FormBuilder, private employeeService: EmployeeService,
              private router: Router, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.initForm();
    this.mutualTypeValidator();
    this.foreignWorkerValidator();
    this.exitDateValidator();
  }

  private initForm(): void {
    this.form = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      birthDate: ['', Validators.required],
      address: this.formBuilder.group({
        contentAddress: ['', Validators.required],
        zipCode: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
        city: ['', Validators.required],
        country: ['', Validators.required],
      }),
      email: this.formBuilder.group({
        email: ['', [Validators.email]]
      }),
      secuNumber: [
        '',
        [
          Validators.pattern(/^[0-9]+$/),
          Validators.minLength(13),
          Validators.maxLength(13),
        ],
      ],
      personalPhoneNumber: [
        '',
        [
          Validators.pattern(/^[0-9]+$/),
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
      contractType: ['', [Validators.required]],
      entryDate: ['', Validators.required],
      hasCompanyMutual: ['', Validators.required],
      mutualType: [null],
      jobType: ['', Validators.required],
      foreignWorker: ['true'],
      exitDate: [''],
      active: this.formBuilder.group({
        isActive: [true]
      }),
      phoneNumber: this.formBuilder.group({
        phoneNumber: [
          '',
          [
            Validators.pattern(/^[0-9]+$/),
            Validators.minLength(10),
            Validators.maxLength(10),
          ],
        ],
      })
    },
      {
        validators: [
          this.dateRangeValidator('entryDate', 'exitDate')
        ]}
    );
  }

  private mutualTypeValidator() {
    this.form.get('hasCompanyMutual')?.valueChanges.subscribe((value) => {
      const mutualTypeControl = this.form.get('mutualType');

      if (value === 'true') {
        mutualTypeControl?.setValidators([Validators.required]);
      } else {
        mutualTypeControl?.clearValidators();
      }

      mutualTypeControl?.updateValueAndValidity();
    });
  }

  private foreignWorkerValidator() {
    this.form.get('foreignWorker')?.valueChanges.subscribe((value) => {
      const secuNumberControl = this.form.get('secuNumber');

      if (value === 'false') {
        secuNumberControl?.setValidators([Validators.required]);
      } else {
        secuNumberControl?.clearValidators();
      }

      secuNumberControl?.updateValueAndValidity();
    });
  }

  private exitDateValidator() {
    this.form.get('contractType')?.valueChanges.subscribe((value) => {
      const exitDateControl = this.form.get('exitDate');

      if (value === 'FIXED_TERM_CONTRACT' || value === 'INTERIM') {
        exitDateControl?.setValidators([Validators.required]);
      } else {
        exitDateControl?.clearValidators();
      }

      exitDateControl?.updateValueAndValidity();
    });
  }

  private dateRangeValidator(startControlName: string, endControlName: string) {
    return (group: FormGroup) => {
      const startControl = group.controls[startControlName];
      const endControl = group.controls[endControlName];

      if (startControl.value && endControl.value && startControl.value > endControl.value) {
        endControl.setErrors({ dateRangeError: true });
        startControl.setErrors({ dateRangeError: true });
      } else {
        endControl.setErrors(null);
      }
    };
  }


  saveEmployee(): Observable<any> {
    const employeeData: Employee = this.form.value;
    return this.employeeService.addEmployee(employeeData);
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  onSubmit(): void {
    if (this.form.valid) {

      this.saveEmployee().subscribe(
        (response) => {
          console.log('Réponse du serveur :', response);
          this.showSuccessMessage("Employé ajouté avec succès !");
          this.router.navigate(['employee-list']);
        },
        (error) => {
          console.error('Erreur lors de la requête', error);
          this.showErrorMessage("Erreur lors de l'ajout de l'employé.");
        }
      );
    }
  }
}

