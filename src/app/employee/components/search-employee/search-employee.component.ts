import { Component, OnInit } from '@angular/core';
import {FormControl, FormBuilder, ReactiveFormsModule} from '@angular/forms';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../classes/employee/employee.model';
import { startWith, map } from 'rxjs/operators';
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {NgForOf, NgIf} from "@angular/common";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-search-employee',
  templateUrl: './search-employee.component.html',
  standalone: true,
  imports: [
    MatInputModule,
    MatIconModule,
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    RouterLink
  ],
  styleUrls: ['./search-employee.component.scss']
})
export class SearchEmployeeComponent implements OnInit {
  searchCtrl!: FormControl;
  searchResults: Employee[] = [];

  constructor(private employeeService: EmployeeService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.initObservables();
  }

  private initForm(): void {
    this.searchCtrl = this.formBuilder.control('');
  }

  private initObservables(): void {
    const search$ = this.searchCtrl.valueChanges.pipe(
      startWith(this.searchCtrl.value),
      map(value => value.toLowerCase())
    );

    this.employeeService.getAllEmployeesDTO().subscribe(employees => {
      search$.subscribe(search => {
        this.searchResults = employees.filter(employee =>
          (employee.firstName && employee.firstName.toLowerCase().includes(search)) ||
          (employee.lastName && employee.lastName.toLowerCase().includes(search))
        );
      });
    });
  }
}
