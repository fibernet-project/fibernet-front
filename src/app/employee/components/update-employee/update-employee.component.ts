import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from "../../classes/employee/employee.model";
import { EmployeeService } from "../../services/employee.service";
import {MatInputModule} from "@angular/material/input";
import {JsonPipe, Location, NgForOf, NgIf} from "@angular/common";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatSelectModule} from "@angular/material/select";
import {MatButtonModule} from "@angular/material/button";
import {MatNativeDateModule} from "@angular/material/core";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatRadioModule} from "@angular/material/radio";

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  standalone: true,
  styleUrls: ['./update-employee.component.scss'],
    imports: [
        ReactiveFormsModule,
        MatInputModule,
        NgIf,
        MatDatepickerModule,
        MatSelectModule,
        MatButtonModule,
        MatNativeDateModule,
        JsonPipe,
        ButtonComponent,
        NgForOf,
        MatRadioModule
    ],
})
export class UpdateEmployeeComponent implements OnInit {
  employeeId: string | null = null;
  employeeDetails: Employee | undefined;
  employeeForm: FormGroup;
  protected requiredMessage: string = 'Champ obligatoire';
  protected onlyNumberMessage: string = 'Veuillez saisir des chiffres uniquement';
  protected lengthSecuNumberMessage: string = 'Le numéro doit avoir 13 chiffres';
  protected lengthTelNumberMessage: string = 'Le numéro doit avoir 10 chiffres';
  protected invalidEmailMessage: string = 'Adresse email invalide';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeeService: EmployeeService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private location: Location
  ) {
    this.employeeForm = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      birthDate: ['', Validators.required],
      address: this.formBuilder.group({
        contentAddress: ['', Validators.required],
        zipCode: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
        city: ['', Validators.required],
        country: ['', Validators.required],
      }),
      email: this.formBuilder.group({
        email: ['', [Validators.email]],
      }),
      secuNumber: [
        '',
        [
          Validators.pattern(/^[0-9]+$/),
          Validators.minLength(13),
          Validators.maxLength(13),
        ],
      ],
      personalPhoneNumber: [
        '',
        [
          Validators.pattern(/^[0-9]+$/),
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
      contractType: ['', [Validators.required]],
      entryDate: ['', Validators.required],
      hasCompanyMutual: ['', Validators.required],
      mutualType: [null],
      jobType: ['', Validators.required],
      foreignWorker: ['true'],
      exitDate: [''],
      active: this.formBuilder.group({
        isActive: ['']
      }),
      phoneNumber: this.formBuilder.group({
        phoneNumber: [
          '',
          [
            Validators.pattern(/^[0-9]+$/),
            Validators.minLength(10),
            Validators.maxLength(10),
          ],
        ]
      })
    });
  }

  ngOnInit(): void {
    this.mutualTypeValidator();
    this.foreignWorkerValidator();
    this.exitDateValidator();
    this.employeeId = this.route.snapshot.paramMap.get('id');
    if (this.employeeId) {
      this.employeeService.getEmployeeById(this.employeeId).subscribe(
        (details) => {
          this.employeeDetails = details;

          this.initializeForm(details);
        },
        (error) => {
          console.error('Erreur lors de la récupération des détails de l\'employé.', error);
        }
      );
    } else {
      console.error('ID de l\'employé non trouvé dans l\'URL.');
    }
  }

  private initializeForm(details: Employee): void {
    this.employeeForm.patchValue({
      lastName: details.lastName,
      firstName: details.firstName,
      birthDate: details.birthDate ? new Date(details.birthDate) : null,
      address: {
        contentAddress: details.address.contentAddress,
        zipCode: details.address.zipCode,
        city: details.address.city,
        country: details.address.country,
      },
      email: {
        email: details.email?.email
      },
      secuNumber: details.secuNumber,
      personalPhoneNumber: details.personalPhoneNumber,
      contractType: details.contractType,
      entryDate: details.entryDate ? new Date(details.entryDate) : null,
      hasCompanyMutual: details.hasCompanyMutual,
      mutualType: details.mutualType,
      jobType: details.jobType,
      foreignWorker: !!details.foreignWorker,
      exitDate: details.exitDate ? new Date(details.exitDate) : null,
      phoneNumber: {
        phoneNumber: details.phoneNumber?.phoneNumber
      }
    });
  }

  private mutualTypeValidator() {
    this.employeeForm.get('hasCompanyMutual')?.valueChanges.subscribe((value) => {
      const mutualTypeControl = this.employeeForm.get('mutualType');

      if (value === true) {
        mutualTypeControl?.setValidators([Validators.required]);
      } else {
        mutualTypeControl?.clearValidators();
      }
      mutualTypeControl?.updateValueAndValidity();
    });
  }

  private foreignWorkerValidator() {
    this.employeeForm.get('foreignWorker')?.valueChanges.subscribe((value) => {
      const secuNumberControl = this.employeeForm.get('secuNumber');

      if (value === false) {
        secuNumberControl?.setValidators([Validators.required]);
      } else {
        secuNumberControl?.clearValidators();
      }

      secuNumberControl?.updateValueAndValidity();
    });
  }

  private exitDateValidator() {
    this.employeeForm.get('contractType')?.valueChanges.subscribe((value) => {
      const exitDateControl = this.employeeForm.get('exitDate');

      if (value === 'FIXED_TERM_CONTRACT' || value === 'INTERIM') {
        exitDateControl?.setValidators([Validators.required]);
      } else {
        exitDateControl?.clearValidators();
      }

      exitDateControl?.updateValueAndValidity();
    });
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  protected goBack(): void {
    this.location.back();
  }

  onSubmit() {
    if (this.employeeForm.valid) {
      let formValue = this.employeeForm.value;

      let updatedEmployee: Employee = {
        lastName: formValue.lastName,
        firstName: formValue.firstName,
        birthDate: formValue.birthDate,
        address: {
          contentAddress: formValue.address.contentAddress,
          zipCode: formValue.address.zipCode,
          city: formValue.address.city,
          country: formValue.address.country
        },
        email: {
          email: formValue.email.email
        },
        secuNumber: formValue.secuNumber,
        personalPhoneNumber: formValue.personalPhoneNumber,
        contractType: formValue.contractType,
        entryDate: formValue.entryDate,
        hasCompanyMutual: formValue.hasCompanyMutual,
        mutualType: formValue.mutualType,
        jobType: formValue.jobType,
        foreignWorker: formValue.foreignWorker,
        exitDate: formValue.exitDate,
        phoneNumber: {
          phoneNumber: formValue.phoneNumber.phoneNumber
        },
        active: {
          isActive: formValue.active.isActive
        }
      };

      this.employeeService.updateEmployee(updatedEmployee, this.employeeId).subscribe(
        () => {
          this.showSuccessMessage('Employé mis à jour avec succès');
          this.router.navigate(['employee-list']);
        },
        (error) => {
          console.error('Erreur lors de la mise à jour de l\'employé.', error);
          this.showErrorMessage("Erreur lors de la modification de l'employé");
        }
      );
    }
  }
}
