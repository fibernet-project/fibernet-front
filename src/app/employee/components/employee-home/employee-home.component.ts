import { Component } from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-employee-home',
  standalone: true,
  imports: [
    ButtonComponent,
    RouterLink
  ],
  templateUrl: './employee-home.component.html',
  styleUrl: './employee-home.component.scss'
})
export class EmployeeHomeComponent {

}
