import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginatorModule, PageEvent} from '@angular/material/paginator';
import {Employee} from '../../classes/employee/employee.model';
import {EmployeeService} from '../../services/employee.service';
import {RouterLink} from '@angular/router';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {NgForOf, NgIf, NgStyle} from "@angular/common";
import {PaginatorComponent} from "../../../shared/components/paginator/paginator.component";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
  imports: [
    MatPaginatorModule,
    ButtonComponent,
    RouterLink,
    NgIf,
    NgForOf,
    PaginatorComponent,
    NgStyle
  ],
  standalone: true
})
export class EmployeeListComponent implements OnInit {
  @ViewChild(PaginatorComponent) paginator: PaginatorComponent | undefined;

  employees: Employee[] = [];
  totalEmployees: number = 0;
  pageSize: number = 5;
  currentPage: number = 1;
  currentPageData: Employee[] = [];

  constructor(private employeeService: EmployeeService) {}

  ngOnInit(): void {
    this.getEmployees();
  }

  getEmployees(): void {
    this.employeeService.getAllEmployees().subscribe(
      (data: Employee[]) => {
        this.employees = data;
        this.totalEmployees = data.length;
        this.updatePageData();
      },
      (error) => {
        console.error('Error fetching employees:', error);
      }
    );
  }

  onPageChanged(event: PageEvent) {
    this.currentPage = event.pageIndex + 1;
    this.updatePageData();
  }

  updatePageData() {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    this.currentPageData = this.employees.slice(startIndex, endIndex);
  }
}
