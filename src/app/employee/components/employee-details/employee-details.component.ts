import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, RouterLink} from "@angular/router";
import { EmployeeService } from "../../services/employee.service";
import {DatePipe, Location, NgForOf, NgIf} from "@angular/common";
import { Employee } from "../../classes/employee/employee.model";
import { displayMutualTypeValue } from "../../../shared/enums/MutualType.enum";
import { displayContractTypeValue } from "../../../shared/enums/ContractType.enum";
import { ButtonComponent } from "../../../shared/components/button/button.component";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatDialog } from '@angular/material/dialog';
import {
  ConfirmationDialogComponent
} from "../../../shared/components/confirmation-dialog/confirmation-dialog.component";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {FormsModule} from "@angular/forms";
import {AuthService} from "../../../auth/service/auth.service";

@Component({
  selector: 'app-employee-details',
  styleUrls: ['./employee-details.component.scss'],
  templateUrl: './employee-details.component.html',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    DatePipe,
    ButtonComponent,
    RouterLink,
    ConfirmationDialogComponent,
    MatSlideToggleModule,
    FormsModule
  ],
})
export class EmployeeDetailsComponent implements OnInit {
  employeeId: string | null = null;
  employeeDetails: Employee | undefined;
  dateFormat: string = 'dd/MM/YYYY';

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private location: Location,
    protected authService: AuthService
  ) {}

  ngOnInit(): void {
    this.employeeId = this.route.snapshot.paramMap.get('id');
    if (this.employeeId) {
      this.employeeService.getEmployeeById(this.employeeId).subscribe(
        (details) => {
          this.employeeDetails = details;
        },
        (error) => {
          console.error('Erreur lors de la récupération des détails de l\'employé.', error);
        }
      );
    }
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  deleteEmployee() {
    if (this.employeeId) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir supprimer cet employé ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.employeeService.deleteEmployeeById(this.employeeId!).subscribe(
            () => {
              this.showSuccessMessage('Employé supprimé avec succès');
              this.router.navigate(['employee-list']);
            },
            (error) => {
              console.error('Erreur lors de la suppression de l\'employé.', error);
              this.showErrorMessage('Erreur lors de la suppression de l\'employé');
            }
          );
        }
      });
    }
  }

  protected goBack() {
    this.location.back();
  }

  updateStateEmployee() {
    if (this.employeeId) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir modifier cet employé ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          const updatedEmployee: Employee = {
            ...this.employeeDetails!,
            active: {
              isActive: !this.employeeDetails?.active?.isActive
            }
          };

          this.employeeService.updateIsActive(updatedEmployee, this.employeeId).subscribe(
            () => {
              if (this.employeeDetails) {
                this.employeeDetails.active!.isActive = !this.employeeDetails.active!.isActive;
              }
              this.showSuccessMessage('Employé mis à jour avec succès');
            },
            (error) => {
              console.error('Erreur lors de la mise à jour de l\'employé.', error);
              this.showErrorMessage('Erreur lors de la mise à jour de l\'employé');
            }
          );
        }
      });
    }
  }

  protected readonly displayMutualTypeValue = displayMutualTypeValue;
  protected readonly displayContractTypeValue = displayContractTypeValue;
}
