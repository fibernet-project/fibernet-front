import {Address} from "../../../shared/classes/address/address.model";
import {MutualType} from "../../../shared/enums/MutualType.enum";
import {Deposit} from "../../../deposit/classes/deposit.model";
import {Refund} from "../../../refund/classes/refund.model";
import {Warning} from "../../../warning/classes/warning.model";
import {ContractType} from "../../../shared/enums/ContractType.enum";
import {Worker} from "../../../shared/classes/worker/worker.model";

export class Employee extends Worker {
  entryDate?: Date;
  hasCompanyMutual?: boolean;
  mutualType?: MutualType;
  address = new Address();
  deposits?: Deposit[];
  refunds?: Refund[];
  warnings?: Warning[];
  contractType?: ContractType;
  secuNumber?: string;
  personalPhoneNumber?: string;
  jobType?: string;
  exitDate?: Date;

  constructor(data: Partial<Employee>) {
    super(data);
    Object.assign(this, data);
  }
}
