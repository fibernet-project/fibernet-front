import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Employee} from "../classes/employee/employee.model";
import {Observable, throwError} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private apiUrl = environment.apiUrl;
  private endPoint : string = '/employees';

  constructor(private httpClient: HttpClient) {}

  public addEmployee(employee : Employee) : Observable<Object> {
    return this.httpClient.post(`${this.apiUrl + this.endPoint}`, employee);
  }
  public getAllEmployees() : Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(`${this.apiUrl + this.endPoint}`);
  }

  getEmployeeById(id: string): Observable<Employee> {
    return this.httpClient.get<Employee>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  deleteEmployeeById(id: string): Observable<void> {
    return this.httpClient.delete<void>(`${this.apiUrl + this.endPoint}/${id}`);
  }
  updateEmployee(employee: Employee, id: string | null): Observable<Object> {

    if (id === null) {
      console.error("L'employé doit avoir un ID pour être mis à jour.");
      return throwError("L'employé doit avoir un ID pour être mis à jour.");
    }

    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}`, employee);
  }
  updateIsActive(employee: Employee, id: string | null): Observable<Object> {
    if (id === null) {
      console.error("L'employé doit avoir un ID pour être mis à jour.");
      return throwError("L'employé doit avoir un ID pour être mis à jour.");
    }

    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}/isActive`, employee);
  }

  public getAllEmployeesDTO() : Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(`${this.apiUrl + this.endPoint}/dto`);
  }
}
