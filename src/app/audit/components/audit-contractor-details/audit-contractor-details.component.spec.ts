import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditContractorDetailsComponent } from './audit-contractor-details.component';

describe('AuditContractorDetailsComponent', () => {
  let component: AuditContractorDetailsComponent;
  let fixture: ComponentFixture<AuditContractorDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AuditContractorDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AuditContractorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
