import {Component, OnInit} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {DatePipe, Location, NgForOf, NgIf, NgStyle} from "@angular/common";
import {ActivatedRoute, RouterLink} from "@angular/router";
import {ContractorService} from "../../../contractor/services/contractor.service";
import {Contractor} from "../../../contractor/classes/contractor/contractor.model";
import {MatIconModule} from "@angular/material/icon";
import {AuditInformations} from "../../../technician/classes/audit-informations/audit-informations.model";

@Component({
  selector: 'app-audit-contractor-details',
  standalone: true,
  imports: [
    ButtonComponent,
    DatePipe,
    NgIf,
    RouterLink,
    NgStyle,
    MatIconModule,
    NgForOf
  ],
  templateUrl: './audit-contractor-details.component.html',
  styleUrl: './audit-contractor-details.component.scss'
})
export class AuditContractorDetailsComponent implements OnInit {

  dateFormat: string = 'dd/MM/YYYY';
  showTechnicians: boolean = false;
  buttonText!: string;
  constructor(private route: ActivatedRoute,
              private contractorService: ContractorService,
              private location: Location
  ) {
  }

  contractorId: string | null = null;
  contractorDetails: Contractor | undefined;

  ngOnInit(): void {
    this.contractorId = this.route.snapshot.paramMap.get('id');
    if (this.contractorId) {
      this.contractorService.getContractorById(this.contractorId).subscribe(
        (details) => {
          this.contractorDetails = details;
        },
        (error) => {
          console.error("Erreur lors de la récupération du prestataire.", error);
        }
      )
    }
  }
  toggleTechnicians() {
    this.showTechnicians = !this.showTechnicians;
  }

  goBack() {
    this.location.back();
  }

  showWarningIcon(auditInformations: AuditInformations | undefined) : boolean {
   return (!auditInformations?.hasCACES || !auditInformations?.professionalCard || !auditInformations?.qualifications ||
     !auditInformations?.aiprAuthorization || !auditInformations?.pprPlan || !auditInformations?.vehicleMarking ||
     !auditInformations?.personalProtectiveEquipment || !auditInformations?.equipment);
  }

}
