import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditTechnicianDetailsComponent } from './audit-technician-details.component';

describe('AuditTechnicianDetailsComponent', () => {
  let component: AuditTechnicianDetailsComponent;
  let fixture: ComponentFixture<AuditTechnicianDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AuditTechnicianDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AuditTechnicianDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
