import {Component, OnInit} from '@angular/core';
import {TechnicianService} from "../../../technician/services/technician.service";
import {Technician} from "../../../technician/classes/technician/technician.model";
import {ActivatedRoute, RouterLink} from "@angular/router";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {Location, NgIf} from "@angular/common";
import {displayTechnicianRating} from "../../../shared/enums/TechnicianRating.enum";

@Component({
  selector: 'app-audit-technician-details',
  standalone: true,
  imports: [
    ButtonComponent,
    NgIf,
    RouterLink
  ],
  templateUrl: './audit-technician-details.component.html',
  styleUrl: './audit-technician-details.component.scss'
})
export class AuditTechnicianDetailsComponent implements OnInit {

  technicianId: string | null = null;
  technicianDetails: Technician | undefined;

  constructor(private technicianService: TechnicianService,
              private route: ActivatedRoute,
              private location: Location) {
  }
  ngOnInit(): void {
    this.technicianId = this.route.snapshot.paramMap.get('id');
    if (this.technicianId) {
      this.technicianService.getTechnicianById(this.technicianId).subscribe(
        (details) => {
          this.technicianDetails = details;
        },
        (error) => {
          console.error('Error fetching technician', error);
        }
      );
    }
  }

  protected goBack() {
    this.location.back();
  }

  protected readonly displayTechnicianRating = displayTechnicianRating;
}
