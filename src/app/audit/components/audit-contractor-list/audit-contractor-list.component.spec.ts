import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditContractorListComponent } from './audit-contractor-list.component';

describe('AuditContractorListComponent', () => {
  let component: AuditContractorListComponent;
  let fixture: ComponentFixture<AuditContractorListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AuditContractorListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AuditContractorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
