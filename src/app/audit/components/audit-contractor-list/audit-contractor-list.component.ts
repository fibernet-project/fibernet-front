import {Component, OnInit} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {RouterLink} from "@angular/router";
import {NgForOf, NgIf, NgStyle} from "@angular/common";
import {ContractorService} from "../../../contractor/services/contractor.service";
import {Contractor} from "../../../contractor/classes/contractor/contractor.model";

@Component({
  selector: 'app-audit-contractor-list',
  standalone: true,
  imports: [
    ButtonComponent,
    RouterLink,
    NgStyle,
    NgIf,
    NgForOf
  ],
  templateUrl: './audit-contractor-list.component.html',
  styleUrl: './audit-contractor-list.component.scss'
})
export class AuditContractorListComponent implements OnInit {

  constructor(private contractorService: ContractorService) {
  }

  contractors: Contractor[] = [];

  ngOnInit(): void {
    this.getAllCompanies();
  }

  getAllCompanies() {
    this.contractorService.getAllContractors().subscribe(
      (data: Contractor[]) => {
        this.contractors = data;
      },
      (error) => {
        console.error('Erreur lors de la récupération d\'une company', error);
      }
    );
  }

}
