import {Component, OnInit} from '@angular/core';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {NgForOf, NgIf} from "@angular/common";
import {FormBuilder, FormControl, ReactiveFormsModule} from "@angular/forms";
import {Technician} from "../../../technician/classes/technician/technician.model";
import {TechnicianService} from "../../../technician/services/technician.service";
import {map, startWith} from "rxjs/operators";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-audit-search-technician',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    RouterLink
  ],
  templateUrl: './audit-search-technician.component.html',
  styleUrl: './audit-search-technician.component.scss'
})
export class AuditSearchTechnicianComponent implements OnInit {
  searchCtrl!: FormControl;
  searchResults: Technician[] = [];

  constructor(private technicianService: TechnicianService,
              private formBuilder: FormBuilder) {
  }
  ngOnInit(): void {
    this.initForm();
    this.initObservables();
  }

  private initForm(): void {
    this.searchCtrl = this.formBuilder.control('');
  }

  private initObservables(): void {
    const search$ = this.searchCtrl.valueChanges.pipe(
      startWith(this.searchCtrl.value),
      map(value => value.toLowerCase())
    );

    this.technicianService.getAllTechnicians().subscribe(technicians => {
      search$.subscribe(search => {
        this.searchResults = technicians.filter(technician =>
          (technician.firstName && technician.firstName.toLowerCase().includes(search)) ||
          (technician.lastName && technician.lastName.toLowerCase().includes(search))
        );
      });
    });
  }
}
