import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditSearchTechnicianComponent } from './audit-search-technician.component';

describe('AuditSearchTechnicianComponent', () => {
  let component: AuditSearchTechnicianComponent;
  let fixture: ComponentFixture<AuditSearchTechnicianComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AuditSearchTechnicianComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AuditSearchTechnicianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
