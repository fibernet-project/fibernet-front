import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Location, NgIf} from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ButtonComponent } from '../../../shared/components/button/button.component';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { ReactiveFormsModule } from '@angular/forms';
import {TechnicianService} from "../../../technician/services/technician.service";
import {Technician} from "../../../technician/classes/technician/technician.model";
import {ActivatedRoute, Router} from "@angular/router";
import {MatFormFieldModule} from "@angular/material/form-field";

@Component({
  selector: 'app-audit-technician',
  standalone: true,
  imports: [
    MatCheckboxModule,
    ButtonComponent,
    MatInputModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    NgIf
  ],
  templateUrl: './audit-technician.component.html',
  styleUrls: ['./audit-technician.component.scss'],
})
export class AuditTechnicianComponent implements OnInit {

  form!: FormGroup;
  technicianId: string | null = null;
  technicianDetails: Technician | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private technicianService: TechnicianService,
    private snackBar: MatSnackBar,
    private location: Location,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.form = this.formBuilder.group({
      auditInformations: this.formBuilder.group({
        auditComment: [''],
        professionalCard: [false],
        qualifications: [false],
        aiprAuthorization: [false],
        pprPlan: [false],
        vehicleMarking: [false],
        personalProtectiveEquipment: [false],
        equipment: [false],
        workMethodology: [false],
        process: [false],
        technicianRating: [null],
        hasCherryPicker: [false],
        hasCACES: [false],
        hasVGP: [false],
        hasFlyingCherryPicker: [false],
      }),
    });
  }

  ngOnInit(): void {
    this.technicianId = this.route.snapshot.paramMap.get('id');
    if (this.technicianId) {
        this.technicianService.getTechnicianById(this.technicianId).subscribe(
          (details) => {
            this.technicianDetails = details;
            this.initForm(details);
          },
          (error) => {
            console.error("Erreur lors de la récupération des détails du technicien", error);
          }
        )
    } else {
      console.error("ID du technicien non trouvé dans l\'URL");
    }

  }
  private initForm(details: Technician): void {
    this.form.patchValue({
      ...details,
      auditInformations: {
        professionalCard: details.auditInformations?.professionalCard,
        qualifications: details.auditInformations?.qualifications,
        aiprAuthorization: details.auditInformations?.aiprAuthorization,
        pprPlan: details.auditInformations?.pprPlan,
        vehicleMarking: details.auditInformations?.vehicleMarking,
        personalProtectiveEquipment: details.auditInformations?.personalProtectiveEquipment,
        equipment: details.auditInformations?.equipment,
        workMethodology: details.auditInformations?.workMethodology,
        process: details.auditInformations?.process,
        hasCherryPicker: details.auditInformations?.hasCherryPicker,
        hasVGP: details.auditInformations?.hasVGP,
        hasCACES: details.auditInformations?.hasCACES,
        hasFlyingCherryPicker: details.auditInformations?.hasFlyingCherryPicker,
        technicianRating: details.auditInformations?.technicianRating,
        auditComment: details.auditInformations?.auditComment
      }
    });
  }
  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  protected goBack(): void {
    this.location.back();
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.technicianId) {
        let formData = this.form.value;
        let auditTechnician: Technician = {
          auditInformations: {
            auditComment: formData.auditInformations.auditComment,
            professionalCard: formData.auditInformations.professionalCard,
            qualifications: formData.auditInformations.qualifications,
            aiprAuthorization: formData.auditInformations.aiprAuthorization,
            pprPlan: formData.auditInformations.pprPlan,
            vehicleMarking: formData.auditInformations.vehicleMarking,
            personalProtectiveEquipment:
            formData.auditInformations.personalProtectiveEquipment,
            equipment: formData.auditInformations.equipment,
            workMethodology: formData.auditInformations.workMethodology,
            process: formData.auditInformations.process,
            technicianRating: formData.auditInformations.technicianRating,
            hasCherryPicker: formData.auditInformations.hasCherryPicker,
            hasCACES: formData.auditInformations.hasCACES,
            hasVGP: formData.auditInformations.hasVGP,
            hasFlyingCherryPicker: formData.auditInformations.hasFlyingCherryPicker,
          }
        }

      console.log('audit tech : ' + auditTechnician.firstName);
      this.technicianService.updateAuditTechnician(auditTechnician, this.technicianId).subscribe(
        (response) => {

          this.showSuccessMessage('Technicien audité avec succès !');
          console.log('Technicien ajouté avec succès !', response);
          this.router.navigate(['/audit-technician-details', this.technicianDetails?.id]);
        },
        (error) => {
          console.error('Erreur lors de l\'ajout de l\'audit', error);
          this.showErrorMessage('Erreur lors de l\'ajout de l\'audit');
        }
      );
    }
  }
  }
}
