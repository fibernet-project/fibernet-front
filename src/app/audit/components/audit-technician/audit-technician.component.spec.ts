import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditTechnicianComponent } from './audit-technician.component';

describe('AuditTechnicianComponent', () => {
  let component: AuditTechnicianComponent;
  let fixture: ComponentFixture<AuditTechnicianComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AuditTechnicianComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AuditTechnicianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
