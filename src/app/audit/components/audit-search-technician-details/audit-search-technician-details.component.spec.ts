import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditSearchTechnicianDetailsComponent } from './audit-search-technician-details.component';

describe('AuditSearchTechnicianDetailsComponent', () => {
  let component: AuditSearchTechnicianDetailsComponent;
  let fixture: ComponentFixture<AuditSearchTechnicianDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AuditSearchTechnicianDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AuditSearchTechnicianDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
