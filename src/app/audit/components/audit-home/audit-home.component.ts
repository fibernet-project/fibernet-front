import { Component } from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-audit-home',
  standalone: true,
  imports: [
    ButtonComponent,
    RouterLink
  ],
  templateUrl: './audit-home.component.html',
  styleUrl: './audit-home.component.scss'
})
export class AuditHomeComponent {

}
