import {Component, OnInit} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {NgForOf, NgIf} from "@angular/common";
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {Employee} from "../../../employee/classes/employee/employee.model";
import {EmployeeService} from "../../../employee/services/employee.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {WarningService} from "../../services/warning.service";
import {Warning} from "../../classes/warning.model";
import {MatRadioModule} from "@angular/material/radio";

@Component({
  selector: 'app-add-warning',
  standalone: true,
  imports: [
    ButtonComponent,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatRadioModule,
    MatSelectModule,
    NgIf,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgForOf
  ],
  templateUrl: './add-warning.component.html',
  styleUrl: './add-warning.component.scss'
})
export class AddWarningComponent implements OnInit {
  form!: FormGroup;
  employees: Employee[] = [];
  warnings: Warning[] = [];
  protected requiredMessage: string = 'Champ obligatoire';
  protected emptyEmployeeMessage: string = 'Aucun employé à afficher. Veuillez aller dans le menu "Employés".';

  constructor(private formBuilder: FormBuilder,
              private employeeService: EmployeeService,
              private snackBar: MatSnackBar,
              private router: Router,
              private warningService: WarningService) {
  }

  ngOnInit() {
    this.initForm();
    this.getAllEmployees();
    this.getWarnings();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      date: ['', [Validators.required]],
      employee: ['', [Validators.required]],
      reason: ['', Validators.required],
      number: ['']
    })
  }

  getAllEmployees() {
    this.employeeService.getAllEmployees().subscribe(
      (data) => {
        this.employees = data;
      },
      (error) => {
        console.error('Erreur lors de la récupération des données', error);
      });
  }

  getWarnings() {
    this.warningService.getAllWarnings().subscribe(
      (data) => {
        this.warnings = data;
      },
      (error) => {
        console.error("Erreur lors de la récupération des avertissements.", error);
      }
    )
  }

  calculateWarningNumber(employeeId: number | undefined): number {
    const employeeWarnings = this.warnings.filter(warning => warning.employee?.id === employeeId);
    return employeeWarnings.length + 1;
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  saveWarning(): Observable<any> {
    const warningData: Warning = this.form.value;
    warningData.number = this.calculateWarningNumber(warningData?.employee?.id);
    return this.warningService.addWarning(warningData);
  }

  onSubmit() {
    if (this.form.valid) {
      this.saveWarning().subscribe(
        (response) => {
          console.log('Réponse du serveur : ', response);
          this.showSuccessMessage('Avertissement ajouté avec succès !');
          this.router.navigate(['warning-list']);
        },
        (error) => {
          console.error('Erreur lors de la requête', error);
          this.showErrorMessage('Erreur lors de l\'ajout de l\'avertissement.');
        }
      );
    }
  }
}
