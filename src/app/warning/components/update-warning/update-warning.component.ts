import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {DatePipe, Location, NgIf} from "@angular/common";
import {WarningService} from "../../services/warning.service";
import {Warning} from "../../classes/warning.model";
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";

@Component({
  selector: 'app-update-warning',
  standalone: true,
  imports: [
    ButtonComponent,
    DatePipe,
    FormsModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    NgIf,
    ReactiveFormsModule,
    MatInputModule
  ],
  templateUrl: './update-warning.component.html',
  styleUrl: './update-warning.component.scss'
})
export class UpdateWarningComponent implements OnInit {

  warningId: string | null = null;
  warningDetails: Warning | undefined;
  warningForm!: FormGroup;
  protected requiredMessage: string = 'Champ obligatoire';
  dateFormat: string = 'dd/MM/YYYY';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              private location: Location,
              private warningService: WarningService) {
    this.warningForm = this.formBuilder.group({
      reason: ['', [Validators.required]]
    })
  }

  ngOnInit() {

    this.warningId = this.route.snapshot.paramMap.get('id');

    if (this.warningId) {
      this.warningService.getWarningById(this.warningId).subscribe(
        (details) => {
          this.warningDetails = details;
          this.initializeForm(details);
        },
        (error) => {
          console.error('Erreur lors de la récupération des détails de l\'avertissement.', error);
        }
      );
    } else {
      console.error('ID de l\'avertissement non trouvé dans l\'URL.');
    }
  }


  private initializeForm(details: Warning) {
    this.warningForm.patchValue({
      reason: details.reason
    });
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  protected goBack(): void {
    this.location.back();
  }

  onSubmit() {
    if (this.warningForm.valid) {
      let formValue = this.warningForm.value;

      let updatedWarning: Warning = {
        reason: formValue.reason
      };

      this.warningService.updateReason(updatedWarning, this.warningId).subscribe(
        () => {
          this.showSuccessMessage('Raison de l\'avertissement mise à jour.');
          this.router.navigate(['warning-list']);
        },
        (error) => {
          console.error('Erreur lors de la mise à jour de l\'avertissement.', error);
          this.showErrorMessage("Erreur lors de la modification de l'avertissement.");
        }
      )
    }
  }
}
