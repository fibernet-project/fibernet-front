import {Component, OnInit} from '@angular/core';
import {ButtonComponent} from "../../../shared/components/button/button.component";
import {DatePipe} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {RouterLink} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {
  ConfirmationDialogComponent
} from "../../../shared/components/confirmation-dialog/confirmation-dialog.component";
import {WarningService} from "../../services/warning.service";
import {Warning} from "../../classes/warning.model";
import {InfoDialogComponent} from "../../../shared/components/info-dialog/info-dialog.component";

@Component({
  selector: 'app-warning-list',
  standalone: true,
  imports: [
    ButtonComponent,
    DatePipe,
    MatIconModule,
    RouterLink
  ],
  templateUrl: './warning-list.component.html',
  styleUrl: './warning-list.component.scss'
})
export class WarningListComponent implements OnInit {

  warnings: Warning[] = [];
  dateFormat: string = 'dd/MM/YYYY';

  constructor(
    private warningService: WarningService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.getWarnings();
  }

  showWarningReason(reason: string | undefined) {
    const dialogRef = this.dialog.open(InfoDialogComponent, {
      width: '900px',
      height: '400px',
      data: {
        title: 'Raison',
        message: reason,
        buttonMessage: "OK"
      }
    });
  }


  getWarnings() {
    this.warningService.getAllWarnings().subscribe(
      (data) => {
        this.warnings = data;
      },
      (error) => {
        console.error("Erreur lors de la récupération des avertissements.", error);
      }
    )
  }

  private showSuccessMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['success-snackbar']
    });
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 6000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['error-snackbar']
    });
  }

  deleteWarning(id: number | undefined) {
    if (id) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          titre: 'Confirmation',
          message: 'Êtes-vous sûr de vouloir supprimer cet avertissement ?'
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.warningService.deleteWarningById(id!).subscribe(
            () => {
              this.showSuccessMessage('Avertissement supprimé avec succès');
              this.getWarnings();
            },
            (error) => {
              console.error('Erreur lors de la suppression de l\'avertissement.', error);
              this.showErrorMessage('Erreur lors de la suppression de l\'avertissement.');
            }
          );
        }
      });
    }
  }
}
