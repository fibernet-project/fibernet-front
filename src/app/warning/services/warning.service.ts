import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Warning} from "../classes/warning.model";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class WarningService {

  private apiUrl = environment.apiUrl;
  private endPoint : string = '/warnings';

  constructor(private httpClient: HttpClient) { }

  public addWarning(warning: Warning): Observable<Object> {
    return this.httpClient.post(`${this.apiUrl + this.endPoint}`, warning);
  }

  public getAllWarnings(): Observable<Warning[]> {
    return this.httpClient.get<Warning[]>(`${this.apiUrl + this.endPoint}`);
  }

  public deleteWarningById(id: number) {
    return this.httpClient.delete<void>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  public getWarningById(id: string | undefined): Observable<Warning> {
    return this.httpClient.get<Warning>(`${this.apiUrl + this.endPoint}/${id}`);
  }

  updateReason(warning: Warning, id: string | null): Observable<Object> {

    if (id === null) {
      console.error("L'avertissement doit avoir un ID pour être mis à jour.");
      return throwError("L'avertissement doit avoir un ID pour être mis à jour.");
    }

    return this.httpClient.put(`${this.apiUrl + this.endPoint}/${id}`, warning);
  }
}
