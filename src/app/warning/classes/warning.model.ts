import {BaseEntity} from "../../shared/classes/bases/base-entity.model";
import {Employee} from "../../employee/classes/employee/employee.model";
import {Operations} from "../../shared/classes/operations/operations.model";

export class Warning extends Operations {
  number?: number;
  reason?: string;

  constructor(data: Partial<Employee>) {
    super(data);
    Object.assign(this, data);
  }
}
